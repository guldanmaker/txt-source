『咕姆姆……！好、好硬……！』
「小薩，等等呀。」
薩格基爾試圖咬住肉塊，結果肉塊卻彈開了牠的牙齒。

彼方拿起放在大盤子旁的小刀把肉切開。

「這個要切的薄薄的才可以吃喔。」
即使長年使用的小刀刀刃早已變鈍，彼方依舊輕鬆切開堅硬的肉塊。

被切成薄片的肉塊，依然可見外側燒黑的痕跡，但裡頭卻是通透的粉紅色。

這是公會酒館所自豪的絕品，烤牛肉。

為了配合客人所喜好的口感，可以自行選擇切片或切塊。

「好了，啊。」
『姆，不勝感激。』
薩格基爾大口咬下淋上醬汁的肉。

像這樣被彼方餵食，也漸漸習慣了。

這難道不算墮落嗎。

薩格基爾察覺到危機感。

像這樣被人嬌慣，別說是變強了，反倒還會變成廢物。

但是，對於忍受數百年孤獨的薩格基爾來說，彼方那毫不吝嗇的溫柔反倒令牠十分感激。

順從主人的期望即為好從僕。

所以這也沒辦法。

薩格基爾替自己找了藉口。

『這還真好吃……！剛才明明這麼硬，現在卻……這真的是同樣的食物嗎……！？』
以餘熱緩緩烤熟的烤牛肉，被彼方以那不可思議的小刀切成通透的薄片，放入口中後輕柔融化。

「呼呼─，這裡還有很多喔，盡管吃沒關係。」
彼方把烤牛肉切開之後，到底能做出多少份烤牛肉呢。

廚師從廚房深處窺視彼方，嘴裡甚至嘟噥著『真希望我家也能有一把這樣的小刀……！』
『姆咕姆咕，哈咕哈咕。』
「哈嗚，拚命吃東西的小薩好可愛喔……。我已經看飽了……」
彼方把靠在桌上的手臂當成枕頭，專注看著薩格基爾用餐。

面紅耳赤、雙瞳濕潤，彷彿戀愛中的少女般。

『不，要好好吃飯啊彼方。』
「如果小薩餵我的話，我會考慮看看─」
『姆姆，確實。余總是被人照顧也不太舒服。好，交給余吧。』
薩格基爾以短小的手腳抱住叉子。

總算是把叉子刺入烤牛肉內，打算送往彼方口中，卻因為薩格基爾不太擅長雙腳行走而陷入困難。

『嗚喔！？』
圓滾滾的身體滾了下去。

烤牛肉飛向空中，開始自由落體。

「我─要─開─動─，了。」
為了不辜負毛茸茸的好意，彼方反應神速。

以遠超過獵犬的速度咬住烤牛肉，姆咕姆咕的咀嚼。

「好好吃～。小薩，謝謝你！」
『恩、嗯，貴公多吃點吧。』
「那麼，接下來就輪到我了呢。請多吃點喔。」
『姆、姆姆，這實在是有些羞恥。但，既然這是貴公所期望的，余也只好接受了。』
然而，就在兩人於餐桌上親親我我時，酒館旁的公會接待處正忙得不可開交。

原因，當然就是因為彼方。

前幾天才剛解決懸賞高額獎金的魔鳥兄弟，隔天又討伐了巨龍。

還在處理其獎金時，又達成新的委託。

而且那達成的委託，又是會讓人懷疑自己聽錯的內容。

彼方所接下的委託是簡單的下水道清掃才對。

然而，她卻不知為何完全淨化了整個王都的下水道，甚至制止了讓下街充斥毒的原因，還將堆積了大量咒怨而變化為物理性毒物，並且四
散的惡靈升天。

要是誰也不接下這項委託，持續把下水道放置不管，惡靈的力量就會持續增強，從無防備的地下襲向王都。

彼方所做的事甚至等同於救了王都居民的未來。

彼方在短時間內持續達成英雄偉業，而為了確認事實與報酬，公會正鬧得沸沸揚揚。

而且目前還是晚餐時間。

一到這個時間，許多的冒險者都會完成委託回到街上。

不管哪個接待櫃台前都排滿人潮，冒險者們也只能焦急等著輪到自己。

「為什麼今天這麼慢啊……？我的肚子都快餓扁了。」
為了不因插隊而降低公會對自身的印象，粗魯的冒險者們也只能老實待在這裡排隊。

但是，一直到了晚餐時間隊伍都沒有前進，焦急的情緒已經完全消去的男子垂下肩膀。

「你不知道嗎？那邊那位小姑娘完成了件不得了的委託喔。」
「啊？……咦，那小姑娘不是早上過來的傢伙嗎？！」
「幸好我沒有隨便出手。我還聽說她要跳級到B等級了。」
「取得冒險者資格的當天就升級，簡直是和萬年Ｄ等級的我不同。這到底是誰啊。」
「彼方˙阿爾戴亞，這名字你多少也聽過吧。我也沒想到她居然是這麼可愛的小姑娘。如果是那個小姑娘，會升級也是理所當然的。嘛，就是這樣。所以暫時輪不到我們。」
「啊─，原來那些不先去接待處就開始喝酒的傢伙，是因為知道這件事啊……。該死，要是我也先過去就好了……」
男子不爽地注視著前方那些早早放棄排隊，開始喝酒的冒險者們。

男子一直排隊，好不容易才排到中間的位置，現在要離隊也覺得有點吃虧。

「真是的，到底要做到什麼時候啊。差不多該換人了吧。」
男子從隊伍中探出頭窺視前方的樣子，卻發現不是冒險者而是身穿做工精良服裝的中年男子正對著接待小姐低下頭。

「無論如何！無論如何還請您稍等一陣子！」
在眾多的吵鬧聲中，就只有男性的呼喊特別大聲。

「委託費一定會支付！只是目前的預算實在是不夠支付！」
深深低下頭的人是對公會提出清掃下水道委託的官廳負責人。

在官廳聽見自己所提出的委託完成的報告，看見上頭所寫的委託費用的負責人不禁失笑。

清掃下水道的委託是以步計算。

一步的距離是三枚銅幣。

或許是條件太差了，被放置了很長一段時間都沒有人願意接受，好不容易才有冒險者願意接受。

本任務是以步計算，所以並沒有事先把委託金交給公會。

因此，他才會為了支付委託金的請求而來。

但是，提出的委託費卻差了五位數。

是怎樣啊，這亂七八糟的金額。

只去掃除個下水道哪可能會有這種金額。

別唬人了。

公會真的有好好工作嗎。

這種紀錄失誤，就連我們這裡的新人都不會犯啊。

官員在一個小時前就來索賠了。

從接待小姐那裡聽說真相的官員臉色蒼白。

公會的報告別說是沒有錯了，甚至不光委託的下水道，就連王都的下水道也全數被淨化。

要是這是事實的話，官廳數年份的都市事業工作就解決了。

光是如此，就已經佔了不少便宜。

官廳所提出委託的是下街的下水道清掃，並非王都全區域的淨化，因此他們並沒有支付下街外範圍委託金的義務。

要是想清掃王都整個下水道的話，恐怕需要以數千人為單位和給予教會大量的捐獻使用好幾次淨化魔法才有可能。

要是這樣子，所需要的花費恐怕會超過數萬枚金幣。

然而，公會此時所需要的支付金額只有六百枚金幣。

從成果上來考慮，甚至比較便宜。問題是沒辦法立刻付清。

當季的預算根本不能擠出來，不過既然委託已經完成，要是沒辦法付清就會違反契約。

與國家法律無關，公會能獨立裁決違反者。

不管對方是官廳的官員還是如何都無所謂。

對此，負責人也相當害怕。

「我非常明白這樣非常不講理！但是，目前金庫內並沒有能夠支付委託的金錢！」
「嗯、嗯，是啊……」
對於深深低下頭的官員，負責應對的職員梅莉莎不爽地隨口回應。

無法支付達成委託的報酬。

這句話好像在哪裡聽說過。

「(話說，這根本就是指我們吧……)」
彼方打倒龍所獲得的獎金變得有些曖昧。

雖然本人說不需要，也沒辦法讓周圍的人接受。

公會目前依然沒有想到能讓所有人都能接受的解決辦法。

和目前這位官員處於同樣狀況的梅莉莎，實在是沒辦法強硬表示。

公會向委託人和冒險者保證雙方委託。

完成委託人的委託。

支付冒險者報酬。

要是委託人像這樣不願意付款的情況下，是能強行徵收金錢，但自己目前實在沒有立場說這種話。

「但是，即便您這樣說，我們也沒辦法打破公會的規定……」
雖然沒有說出這句話的立場，但梅莉莎作為職員也只能這樣說。

「這點還請通融！近期能夠支付的金額，無論怎麼收集也只有一百枚金幣！剩餘的一定會用下一期的預算中支付，能等到那個時候嗎！」
「不，所以說……」
「拜託您了！拜託您了！」
官員用頭摩擦地板如此懇求。

從東方傳至，最高級的謝罪方式，土下座(下跪)。

稍微離題。他的叔父正是露露阿爾斯女子學園的學園長。

「那個，就算您這樣做我也會很困擾。我只不過是普通的職員……」
「拜託您了！拜託您了！」
「好啊？」
從旁邊突然出現的是彼方本人。

一聽見後方傳來的聲音，快哭了的官員突然回過頭去。

「剛才，是如何……？」
「我剛才說可以啊。」
「真、真的嗎？您說的是真的嗎！？」
「是啊，不過，作為代替──」
彼方所說出的條件，正是改善下街的計畫。

下街的居民並沒有作為勞動力和消費者的機能，也是因為中街的居民不願意雇傭下街的居民，甚至拒絕販售商品。

停止不當對待，提高居民的生活水準治安也能提高，又能獲得稅收簡直是一石二鳥。

中街以上的人們和下街的居民或許會有爭執和遺恨，不過這一點已經由彼方的介入解決了。

下街的人們目前已是彼方教的信仰者。

若是如同聖女般崇拜的彼方所說，他們一定會高興地接受。

別搶劫、別強暴，認真工作。

之後就只要整頓好環境，就能順利運轉。

計畫的預算是從本次委託所要支付的金幣中，扣除要馬上支付的一百枚金幣外的五百枚金幣。

「非常感謝您！非常感謝您！敝人必定會照彼方大人所希望的來推動計畫！」
官員將自己的感謝連同頭部一起重複做了幾次表示感謝的土下座。

「彼方桑，我或許不該這樣說，可是這樣子真的好嗎？」
正在處理手續的梅莉莎如此問道。

接著，彼方笑容滿面地回答。

「當然好啦。」
畢竟，只要施予恩惠，不就確實能增加碰上毛茸茸的機率嗎。

這句話雖然沒說出口，卻沒有任何變化。