一直追逐著安麗葉特生活下去的歐緹麗耶，並不是完全沒有作為軍人的自尊心。

投入戰爭的不是平時努力訓練的士兵，而是艾奇德娜創造的奇美拉。
雖然不會造成人員傷亡、成本降到最低限度，聽起來不錯，但是軍士的士氣卻很低落。

「沒想到會把王国的命運托付給這種怪物⋯⋯」

歐緹麗耶喃喃自語。
王城前廣場上排成長隊的奇美拉群。
群眾遠遠地望著這個景象。

「看來王都的居民們也和歐緹麗耶的想法一樣。」

就站在旁邊安麗葉特說道。
看熱鬧的人的表情，總有些不安。
勇士們的逃亡事件給王都帶來了巨大的混亂。
薩托基到現在為止是無條件地被信賴的，不過，對他抱有懷疑的人也開始增多了。
同時，對於他所帶來的奇美拉這個存在的疑問也在不斷增加。

尚未被捕的薇爾希趁機散佈了更多報紙。
以前雖然被無視了，但是最近拿報紙的人好像也增多了。
但是，作為個人活動，最近的舉動太顯眼了。
也流傳著哥哥的利齊是否在幫忙的傳聞，但事實與否尚不明確。
如果薩托基願意的話，查明真相應該很容易吧。
但是現在他沒有那麼多的餘地。

「無論從誰的眼光來看，焦躁都是顯而易見的。雖說是薩托基大人，但也不能完全敷衍過去。」

出擊比原本的預定提前了一步。
芙拉姆他們離開王是五天前。
能夠攜帶的大規模控制裝置的開發本身已經完成了，但是量產還來不及，在現狀中只有兩個可以被使用。
本來計劃在裝置被破壊時設置好幾個設備作為保險──薩托基等不到那一刻。
是想要功勞嗎。
在被逼到絶境之前，忘記了疑惑的人們的，偉大功勞。

「作為友方，還是饒了我吧」

安麗葉特悄悄地著發牢騷。
歐緹麗耶驚訝地看著她。

「姐姐大人會說這種話，真稀奇啊」
「應該由人類來支配大陸的這種想法至今仍未改變，但是──提前出擊，完全是薩托基大人的個人問題吧？確實，如果以閃電般的部署超過一千個奇美拉的話，是會取得勝利的。但是，魔族也不可能不採取任何對抗手段。更何況，當英雄們也站在那邊。」

安麗葉特自己也無法想像如何填補這股戰鬥力的差距。
但是萬一發生什麼事情──

「即使他們有反轉的這個招數，也只是破壊控制裝置而已。而他們應該不知道它的存在。而且，就算注意到了，要突破遍佈地面和空中的奇美拉軍隊幾乎是不可能的。即使是奇跡般地逃脫，也必須與作為控制裝置護衛的奇美拉部隊為敵。」

出擊的人類，只有保護控制裝置的極少一部分人。
軍方的安麗葉特，歐緹麗耶，赫爾曼，維爾納。
教堂騎士團的邱古和巴特。
總共六名，其他的任務全部交給奇美拉。

實際上，即使他們以外的士兵出現在戰場上，也只會成為絆腳石吧。
到現在為止，奇美拉這個存在是絶對的。
但是，雖然理解了這種力量的強大，但是安麗葉特怎麼樣不能給予信任。

「歐緹麗耶，你知道那個叫茵庫的女孩嗎？」
「我聽說她是和芙拉姆他們一起行動的原螺旋之子」
「通過埋入作為心臟的替代品的起源核而獲得起源神力量的螺旋之子──但是她卻不知為何變成了普通人」
「確實是移植了第三者的心臟之類的東西，真是亂來呢。」

安麗葉特笑著說「完全沒錯」

然後繼續說下去。

「Mother包圍王都的時候也是一樣，芙拉姆利用『反轉』的力量在空中飛翔，自己衝進有起源核的某個地方，將其破壊了」
「姐姐，這又怎麼了？」

有點煩躁的歐緹麗耶。
好像不喜歡姐姐談論別的女人。
發現這件事的安麗葉特露出苦笑。

「也不是特別討論芙拉姆・亞普利柯特的事。只是，看著她那個做法⋯⋯覺得這一次，她們也會很荒唐、使用亂來的手段。」
「換句話說，姐姐認為，提前出擊很有可能成為致命的破綻。」
「嗯，是的。我希望不要發生任何事──不管怎麼想，那些傢伙都是敵人啊。不可能不發生任何事吧。」

安麗葉特很確定。

現在的她沒有推遲出擊的權限。
薩托基把所有的權力都集中在自己身上，試圖從起源神手中奪回王国。
她贊同這個想法，並助其一臂之力。
如果是這樣的話，遵照他擬定的計策是理所當然的。

但是──現在的王国，真的可以稱為人類的東西嗎。
看著眼前站著的奇美拉們，她考慮了這個問題。
準備結束之後，裝有轉移裝置的貨車，以及搭乘轉移裝置的歐緹麗耶、維爾納、巴特三人，以及為他們護衛而配備的數個奇美拉，聚集在一處。
軍隊所屬的魔法師們圍著他們。

看不到吉恩的身影，看來到現在也沒有踏出房間一步。
但是即使沒有他的幫助──也可以發動轉移魔法。
同時宣告發動魔法時，歐緹麗耶他們被半透明的藍色穹頂包圍──當它快速收縮時，裡面的物體就會幹淨利落地消失得無影無踪了。


◇◇◇

下一瞬間，歐緹麗耶乘坐的貨車被傳送到沒有寸草不生的荒野的正中央。
不僅是她，維爾納和巴特也暫時精神恍惚地眺望著外面的景色。

「⋯⋯真的，轉移了呢」
「實際這樣體驗的話，我們也只能感到吃驚了」

轉移魔法，終於不再是勇者的特權了。
如果這種技術進一步發展，人們的生活將比現在更方便，王国的霸權將不可動搖吧。

「這裡就是魔族的領地嗎。正如想像的那樣，是一片荒蕪的大地啊」

只生長著孤零零的枯樹，是一片灰色的大地。
在貨車的旁邊，埋藏著琪莉露他們旅行時設置的轉移石。
這裡是她們旅程的終點。
雖然能縮短到目的地賽雷伊德的路程，但是從現在開始，必須用自己的腳移動。
在到達事前會議上指示的地方之前，即使讓奇美拉拉貨車，也需要花兩天時間吧。

「話說回來，為什麼我會和你們在一起？真受不了，這種髒亂的空間呢」
「安麗葉特和歐緹麗耶處在一塊的話，就不知道會發生什麼了。俺覺得這是正確的判斷」
「我只要有姐姐，就能比現在努力一百倍」
「我們沒必要努力吧」

巴特的正論令歐緹麗耶愁眉不展。
事實上，大部分戰鬥都交給奇美拉。
她們只是在控制基米拉的行動的同時，保護著這個裝置罷了。

「話說回來，你⋯⋯新副團長的巴特是嗎？」
「巴特・卡倫」
「全名什麼的都無所謂了。你的臉色看起來比平時好？」

總是一副胃疼的樣子，臉色不好──那是歐緹麗耶所持有的對巴特的印象。
看來維爾納也有相同的看法，「我也這麼想」，同意了。
接受著兩人的視線，他移開視線，小聲說

「因為團長不在」

沒錯這裡沒有那個瘋子邱古。
如此一來，巴特的心會得到多大的安慰呢。
前任副團長杰克是被他「判罪」而殺害的。
聽說在與英雄們的戰鬥中，殺害了無辜的普通人。

總之他超越了巴特的──不，全騎士團員的理解範疇。

「⋯⋯嘛，是這樣一回事啊」

那是連歐緹麗耶都感到同情的悲慘程度。
巴特經常認為，無論擁有怎樣出眾的實力，「那種傢伙」當上騎士團長的地位，不就是教會騎士團的恥辱嗎。
邱古剛當上騎士團長的時候，他似乎還只是個平凡的擁有強烈正義感的男人──但恐怕他已經無法「忍受」了。

他本來就是抑制不了自己的慾望的人。
正因如此，才反覆對婦女施暴，在遇到貞操帶之前甚至都沒有反省過。
但是，即使使用工具進行強制性矯正，也不會從根本上得到改變。
想要摧毀他人，這種慾望，在拘束工具下長年成熟，並持續成長。
結果，在戰鬥中超越了忍耐界限的他，對一般人下手──如果回到王都就會被處分，一定會被解除騎士團長的任職吧。

「不過，他值得信賴也是事實。我聽說和那個加迪歐展開了勢均力敵的戰鬥」

「和那種男人一起行動，真是擔心姐姐大人呢」
「你這傢伙不一直也是那個樣子嗎」

在話題中出場的邱古原本就預定要和安麗葉特、赫爾曼一起行動。
兩個控制裝置分別設置在不同的位置，操縱攻擊賽雷伊德的奇美拉。
換句話說，歐緹麗耶在戰鬥結束之前，甚至連安麗葉特的臉也見不到。
這對她來說是相當大的壓力。
薩托基認為，拉開兩人，總比一起行動導致暴走要好得多。

「為了早日達成令人感動的再會，我必須盡快結束戰鬥。」
「切，我聽不到你的話。算了，我們的目的也是一樣的。如此無聊的戰鬥，必須盡早結束，回到王都」

不是自己的力量，只是操縱奇美拉進攻而已。
除了想遠離邱古的巴特以外，都覺得很無聊。
晚些時候，安麗葉特他們乘坐的貨車也被轉移了，而且奇美拉它們也被傳送了。
轉眼間，周圍布滿了拼湊起來的怪物。
歐緹麗耶他們處於轉移結束，開始進軍之前的空閑。

「聽得見嗎，歐緹麗耶」

在聽到姐姐的聲音的那一瞬間，她撲過去一般地緊緊抓住布袋，往裡面摸索著，取出手掌大小的水晶。
能夠遠程操作奇美拉的話，不也可以遠程傳達聲音嗎──是由艾尼奇德的靈感而製造的通訊裝置。
對話範圍有限，但足以讓兩個部隊互相聯繫。

「姐姐大人，我聽見了！啊，即使相隔很遠，姐姐的聲音還是令人舒適啊！」
「是嗎。那邊也平安轉移真是太好了。馬上就要完成全奇美拉的轉移了，結束之後就要進軍了，千萬不要放鬆警惕。」
「我知道了，為了我和姐姐大人的光明未來！」

在聯繫中斷之前聽到安麗葉特的笑聲不是心理上的錯覺吧。
不知究竟是苦笑呢，還是微笑呢。

「沒有什麼實際內容的會話呢」
「姐姐只是檢查了通信裝置是否能正常運作，內容又沒什麼大不了的」
「即便如此，因為私事而使用的話，我們會怎麼想呢」
「那就跟姐姐大人說，叫邱古過來也行吧？」
「別把我牽扯進來」

巴特的臉色一下子變得蒼白
看到那個的歐緹麗耶咯咯地笑了。
但是在內心，似乎對這個通信裝置的便利性感到驚訝。
艾奇德娜說控制裝置順便製作的，這太荒唐了。
倒不如說這才是主要的。
比起莫名其妙的奇美拉等怪物，這不是革命性的發明嗎。
雖然在成本方面，現階段很難批量生產。

「太可惜了」
「歐緹麗耶的頭嗎？」
「我對姐姐的思念並不浪費大腦資源。是關於艾奇德娜，如果作為一名正派研究者工作的話，我會稱讚她為更偉大的人」

「這是不可能的」
「為什麼」

歐緹麗耶回問。
維爾納冷冷地，不知為何開心地笑了出來。

「那個女人想要的是力量。如果沒有起源核的話，就不會有像現在這樣的成果了」

「這麼說來，是這樣的」

「正派研究者」的前提是錯的。

正因為艾奇德娜不正經，才成為了一名研究者。
人，要擁有比別人更優秀的知識和強大的力量──大腦的某處，必須壊掉。


◇◇◇

奇美拉到齊，控制裝置的動作測試結束後，王国軍開始進軍。

被獅子型牽引的貨車，以普通馬車數倍的速度在荒野上奔馳。
數以千計的奇美拉包圍著它的四周。
異形的生物們齊步前進的樣子，從位於其中心的歐緹麗耶和安麗葉特來看也是異樣的。
每當雙腳踏在地面上，地面就會響起。
若是我方的話，雖然會受到鼓舞，但一想到這群人落入敵手時──就會不由得戰慄起來。

「害怕嗎？」
「是的，維爾納看到這個奇美拉不覺得可怕嗎？」
「是那個嗎，嗯，不會哦。我覺得很棒」
「無法感同身受呢」
「倒不如說，俺們和歐緹麗耶能共感才讓人吃驚呢」

會話並沒有持續很長時間，馬上就打斷了。
貨車劇烈搖晃，像喝醉了酒一樣，巴特的臉色逐漸變差了。
但是──無論前進怎樣，都看不到魔族的身影。
從轉移的瞬間開始，軍隊就一直保持警戒，可是連埋伏都沒有，這是為什麼呢。

不過，即使潛伏著，也無法逃離奇美拉的探知。

「安靜到令人毛骨悚然」

歐緹麗耶在幾小時內一直保持沉默。

「你不是放棄了嗎？」
「姐姐大人說過──是不會不做任何行動的」
「又是姐姐⋯⋯嘛，關於這點我們也有同感。出乎意料地打算在圍城戰什麼的中展開攻勢。」

在途中發現的魔族村落，現在也全都是空殻了。
大概是事先進入賽雷伊德避難了吧。

「在奇美拉的魔力面前進行防守戰，真是愚蠢的選擇啊。」

關於獅子型和飛龍型，因為是巨大的身軀，所以不太擅長一對一的近戰。
即便如此，也不至於不擅長──以與身體不相稱的高敏捷，以及人類只要被擦到就會四分五裂的筋力，如果是普通對手的話，都無法與之戰鬥吧。
但，發揮本領的是，使用遠程魔法進行打擊。

無論疊加多少重的圍牆或屏障，即使固守城池，只要以圍住賽雷伊德的數百個奇美拉集中的炮火為對手，恐怕是無法長時間承受的吧。

「但是時間越長，薩托基大人的處境就越不利。」
「這是一個不值得賭上性命的成果啊，如果是在下的話，會更加摸索別的辦法」
「什麼樣的」
「那是⋯⋯還沒沒有想到。」

總而言之，是學安麗葉特說話。
芙拉姆他們肯定用了無法想像的手段。
在他倆坦然交談的旁邊，巴特臉色蒼白地躺著。
雖然馬車上也有暈車的人，在出發前，他應該誇口說過「肯定沒事的」，但是。
被奇美拉牽引的貨車引起的顛簸比想像的要嚴重吧。

「巴特，你真的沒事嗎？聽說防御方面比其他人都優秀，所以才偏重了姐姐大人那方的戰鬥力呢。」
「沒有⋯⋯問題⋯⋯」

在王都被琪莉露爽快地無視了，但是關於特定場所、特定人物的保護，沒有比巴特更強的了。
即使是使用勇氣的琪莉露，要突破那個屏障大概也需要時間吧。
相反，他可以說是完全沒有攻擊的手段。
恰恰是與邱古完全相反，一直專注於守護的執行正義的使者。
維爾納把視線移向窗外。
流淌的風景逐漸從荒野轉變為雪原。


◇◇◇

兩天後，歐緹麗耶他們到達預定位置。

將控制裝置從貨車上卸下，並安裝到地面上。
到這裡為止，魔族發動的襲擊是零。
雖然對過於順利的旅途抱有不安，但還是按照預定部署了奇美拉，等待著安麗葉特的指示。
幾分鐘後，她的聲音從通訊裝置中傳了出來。

「久等了，這邊的布局也完成了」
「那麼」

歐緹麗耶的指尖觸碰直徑不到一米的水晶體。
表面有無數的光線在閃爍──每一個點都表示周圍存在的奇美拉。
而安麗葉特也同樣向水晶伸出手。

「啊，開戰」

當感受到從指尖發出的微量的魔力時，所有的點都會從白色的光芒變成紅色。
奇美拉們一齊從地面和空中開始向暗流涌動前進。
搖曳的大地上，狂風大作。
歐緹麗耶、維爾曼還有巴特目瞪口呆地抬頭看著。

「這樣一來，我們的職責就結束了嗎？」
「現在還不能大意」
「話雖如此，在這種情況下，什麼都做不了」

賽雷伊德完全被包圍了。
看來是用魔法建成的牆，不過那麼一點堅持不了幾分鐘吧。

「真無趣啊⋯⋯」

維爾納深深地感嘆道。
到這時，歐緹麗耶手中的通訊裝置傳來了聲音。

「能聽⋯⋯沙⋯⋯沙沙⋯⋯嗎」

雜音太多了，聽不見。
但是，直到剛才為止，應該已經能夠和歐緹麗耶他們正常交談了。
即使混雜著雜音，歐緹麗耶也明白，那聲音不是安麗葉特的。

「歐緹麗耶，現在，你能聽到了嗎？」
「是的，果然姐姐那邊也是嗎？」
「果然那邊也⋯⋯難道是魔族做了什麼嗎？」

看來那邊也傳來了相似的聲音。
之後，通信裝置再次響起了神秘的聲音。

「聽得到⋯⋯嗎，安麗⋯⋯沙沙⋯⋯大人⋯⋯」

這次比剛才更鮮明了。
也許正在逐漸接近這裡。

「好像在叫姐姐」
「男人的聲音，難道是王都的傳令嗎？」

雖然現在還不能放棄這是魔族的圈套的可能性，但先是安麗葉特嘗試著回答。

「這裡是安麗葉特，怎麼了？」
「啊，太好⋯⋯」

通信的男人，鬆了口氣。
聽那聲音，她似乎察覺到對方是誰了。

「是羅迪嗎？」
「是，是⋯⋯的。安麗葉特⋯⋯人，有⋯⋯報告⋯⋯沙⋯⋯」

甚至特地把士兵送去魔族領地，也不得不傳達的報告到底是什麼。

如果不是特別重要的事情，就不會做到這個地步。
站在維爾納和巴特，安麗葉特身旁的赫爾曼也一臉嚴肅地專注於談話。
邱古帶著出神的表情凝視著自己的劍，但並非沒有聽到。

「薩⋯⋯基大⋯⋯沙沙⋯⋯」
「抱歉羅迪，雜音太多我聽不到了。能再說一遍嗎？」
「請⋯⋯稍等，因為這裡⋯⋯在，場所⋯⋯！」

聲音中斷。
根據位置不同，雜音的進入情況也不同，羅迪似乎正在向容易通信的地方移動著。
幾分鐘後，再次聽到羅迪的聲音。

「安麗葉特大人，能聽見嗎？」

這次聲音很清晰，沒有雜音。

「嗯，能聽見了」
「太好了，這裡應該沒問題了。那麼⋯⋯向你們報告」

這麼一聽，羅迪的聲音在顫抖。
至少不是好報告。
安麗葉特的神情僵硬。
同樣，聽到對話的歐緹麗耶他們也緊張起來了。

「那個，說實話，我也不知道事情會變成什麼樣子，但是只將事實，簡潔的說──」

他大概也理解自己的聲音很難聽懂吧。
因此，緩緩的發音，以及大點的聲音，明確地宣告著。

「薩托基大人已經去世了」

殘酷的宣告響起。

沉默，時間仿彿停止了一樣，在場的全體人員都靜止不動。
連呼吸都忘了，但是只有心臟在砰砰地跳動。
從歐緹麗耶的通訊裝置那邊傳來了嘎吱的響聲。
恐怕是安麗葉特手中的水晶掉在地上了吧。

那是──誰都想像不到的事態

「⋯⋯怎麼，回事？」

歐緹麗耶也渾身無力，她跪在地上。
薩托基接二連三地處決王后、王子和大臣，以將權力集中在自己身上。
並創造了沒有他就無法運營王国的局面。

如果他已經死了──王国，已經。

在困惑與失望的蔓延中，奇美拉它們仍繼續向賽雷伊德進軍。