# novel

- title: 裏切られたSランク冒険者の俺は、愛する奴隷の彼女らと共に奴隷だけのハーレムギルドを作る 1
- title_zh: 被背叛的S級冒險者的我，和愛戀的奴隸們組成了後宮公會 1
- author: 柊咲
- illust:ナイロン
- 翻譯: 菠蘿點火
- 校對: 樂子人
- 圖源: Andromeda (LK&TSDM ID：愛麗絲•莉澤)
- 漢化組: 悠米之翼漢化組 
- source: https://www.lightnovel.us/detail/1088837
- cover: https://imgur.com/E8zjyHV.jpg
- publisher: 集英社
- date: 2019-04-24T20:00:00+08:00
- status: 已完結
- novel_status: 

## illusts

- ナイロン

## publishers

- 集英社

## series

- name: 裏切られたSランク冒険者の俺は、愛する奴隷の彼女らと共に奴隷だけのハーレムギルドを作る

## preface


```
    奴隷売買が日常となっている世界――。
    昔幼なじみだった少女に裏切られたSランク冒険者のエギル。
    その少女が奴隷だったことから、「奴隷」という存在を忌み嫌い避けながら生きてきた。
    しかし偶然訪れた奴隷オークションで、エレノアに一目惚れをし、勢いで落札。
    しかも彼女はコーネリア王国の王女で、冒険者として旅をしている途中、姉や友人たちに罠にはめられ奴隷堕ちさせられたのだった!
    エギルによって救われ、復讐の機会を手にしたエレノアは彼にこう告げる……。
    「わたくしを奴隷商人に売り飛ばした幼馴染を見つけて――殺してほしいのです」
    彼女は自分の命に代えてでも復讐を果たそうとするのだった。
    そして同じように心に深い傷を負ったエギルは、エレノアの気持ちを知り協力することを誓う。
    その一方で「奴隷」×「主人」という新たに生まれた濃密で甘い関係を二人は旅をしながら堪能するのであった……。
    WEBで話題の作品がついに登場! 奴隷美少女救済のエロティックハードファンタジー!

    僅供個人學習交流使用，禁作商業用途
    下載後請在24小時內刪除，LK、TSDM、demonovel不負擔任何責任
    請尊重翻譯、掃圖、錄入、校對的辛勤勞動，轉載請保留資訊
```

## tags

- node-novel
- R18
- 集英社
- SF fantasy
- 異世界
- 轉生

# contribute

- Andromeda

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1


## textlayout

- allow_lf2: false

# link
- [柊咲@【裏ハー】4巻5/22発売 - twitter](https://twitter.com/saki03231205)
- [輕之國度 - 被背叛的S級冒險者的我，和愛戀的奴隸們組成了後宮公會](https://www.lightnovel.us/detail/1088837)
- [集英社 - 裏切られたSランク冒険者の俺は、愛する奴隷の彼女らと共に奴隷だけのハーレムギルドを作る 1](https://books.shueisha.co.jp/items/contents.html?isbn=978-4-08-631304-9)
- [集英社 - 裏切られたSランク冒険者の俺は、愛する奴隷の彼女らと共に奴隷だけのハーレムギルドを作る 1 コミック](https://books.shueisha.co.jp/items/contents.html?isbn=978-4-08-891711-5)
