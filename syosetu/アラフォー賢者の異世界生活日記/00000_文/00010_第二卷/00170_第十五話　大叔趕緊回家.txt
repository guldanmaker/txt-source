克莉絲汀醒來之後，發現自己在一間儉朴的木造房裡。

環顧四周，發現床旁邊放了一張桌子，牆邊的老梳妝台上面，隨意地放了一個沒有插花的花瓶。

她茫然地仰望天花板，但隨著時間過去，她也漸漸想起自身的狀況。

打倒戰蟻之後的事情她完全沒印象。然後，她理解到這裡是她直到今早為止投宿的旅店房間。

「我是怎麼回到這裡的……對了，杰羅斯先生呢！」

她認為唯一的可能性，就是那個外觀看起來可疑無比的魔導士把自己帶到了這裡。

她心想必須致謝而彈起身子，卻馬上感到一陣頭暈目眩。

因為快速升級導致身體產生變化，而且在還沒完成最佳化之前就勉強了自己，所以她才產生了暈眩症狀。結果她又倒回了床上。

「啊唔～～……」

發出奇怪的呻吟，將臉埋進枕頭裡。

想著要站起身子並使力，疲勞感卻先涌現出來，讓她連動都動不了。

『明天再致謝吧。』

勉強自己前去致謝，反而有可能讓對方不知如何是好。

畢竟看來天色已暗，月光甚至已經照進室內。那個魔導士或許也已經休息了。既然如此，現在更應該先盡量休息，讓身體恢復才是。

克莉絲汀重新蓋好毛毯、閉上眼睛，就聽到一樓餐廳傳來人們喧鬧的笑聲。

過了一會兒，或許她真的太疲累了吧，只聽到不起眼的旅店房間內響起了穩定的呼吸聲。

克莉絲汀再次陷入沉睡之中。

◇　◇　◇　◇

隔天早晨，克莉絲汀醒來之後，立刻換好衣服，急忙出門。

步下樓梯，來到樓下的餐廳之後，就看到幾個傭兵正在用早餐。她發現幾個熟人，才又體會到自己獲救了而放心下來。

「克莉絲汀小姐，您醒了啊。身體的狀況還好嗎？」

「你突然從我們眼前消失的時候，我們可真是擔心死了。」

「伊札特、賽爾，我沒事。柯爾薩和索科塔也沒事啊。」

「為了拯救小姐，無論是什麼地方，我們都會趕去的。」

「沒錯。唉，雖然老實說我們真的很慌張。」

見到隨侍的幾個騎士們也平安無事而安心下來的她，才想起自己來這裡有別的目的。

「伊札特，救了我的人上哪去了？」

「您是指那位魔導士先生嗎？他將克莉絲汀小姐交給我們之後就不見人影了呢。我去問問他的女伴看看。」

身為騎士們的領導者的青年，向坐在前面那一桌享用早餐的三位女性搭話。

「小姐們，不好意思，請問你們知不知道那位魔導士先生在哪？克莉絲汀小姐說無論如何都想向他致謝。」

「啊～？大叔喔……這麼說來沒看到他人耶？」

「是啊。該不會還在房間休息吧？」

「啊～叔叔昨晚已經回去了喔？」

「「「什麼？」」」

伊莉絲說杰羅斯昨晚就已經離開了阿哈恩村，不知情的三人聞言異口同聲發出愚蠢的怪聲。

「等等，伊莉絲！杰羅斯先生什麼時候離開的？」

「我昨天看到的時候，他還在這裡吃飯耶？」

「之後他就在這邊抽了根飯後菸，抽完就回去啦。在你們說要回房間休息，上了二樓之後。」

「這裡到桑特魯城要走上半天耶？他瘋了嗎？這根本就是山賊眼中的肥羊……啊，不可能，山賊反而會被幹掉吧。」

「對吧？我不認為山賊可以打贏那個大叔。他可以毫髮無傷的從礦山最底層回來耶？一般的對手根本拿他沒辦法吧。」

杰羅斯雖在深夜離開村莊，但仍然沒人擔心他的安危。畢竟他實在強到犯規，大家反而都覺得「若有人真的殺得了他，那還真想看看是何方神聖」。

就算當事人不在場，大家看待大叔的態度也是很過分。

「杰羅斯先生沒說什麼嗎？」

「嗯～……好像說了『跟貴族牽扯上太麻煩了，我就先閃人吧』之類的話？」

「居然連道謝都沒辦法……請問，你們知道他住在哪嗎？」

「不知道～叔叔不也說自己是『到處留宿的流浪漢』嗎？」

騎士們不知道該對失落的克莉絲汀說什麼。

魔導士多是不在乎他人的自我中心人物，但杰羅斯竟然連少女的致謝都沒聽就消失，給清爽的早晨帶來苦澀沉重的氣氛。

「魔導士多半是些自我中心的人，別太介意。」

沒人知道中年魔導士去哪了。嘉內不禁嘆口氣，出言安慰。

「不過，他救了我的命，我覺得身為一個人，身為貴族……我至少得道個謝才是。」

「不過對象是杰羅斯先生，我認為他根本不在意這些喔？」

「叔叔總是過得很快活，我覺得你沒有必要這麼鑽牛角尖啦～」

雖然克莉絲汀無論如何都想致謝，但既然當事人不在，這願望也無法實現。

不管怎麼安慰、怎麼說，她都顯得非常失落。

中年魔導士只是隨性地速速離開這個村莊。

爽快離去的大叔這時已經來到桑特魯城的大門前了。

一邊抽著菸、一邊哼著歌……

題外話。在這之後，在阿哈恩村的廢礦山正式被認定為迷宮之前，還是花了一點時間。

為了調查而派出熟練的傭兵，並完成幾道手續，總共花了三個月才被認定為是迷宮。阿哈恩村要趁機恢復以往的活力，又花了好幾年。到了那時候，礦山內部變得更加開闊，也漸漸有大量地魔物出沒。

杰羅斯本人並不知道，這是因為大叔使用了可稱為大屠殺的殲滅魔法攻擊所造成的結果。瞬間化成灰的魔物極有效率地被迷宮給吸收，成為賦予新力量的基石。這座迷宮今後也會持續擴大，直到迷宮的核心被破壞之前，將會變成許多魔物與人類相爭的生活之中，不可或缺的賺錢好所在……

這是後來這座迷宮被稱為「阿哈恩大迷宮」沒多久前發生的事。

◇　◇　◇　◇

時間稍稍回溯，場所來到桑特魯的酒吧。

這邊聚集了粗俗的男人們，各自點了酒，邊喝邊聊著愚蠢的話題。有時也會吵架引起騷動。幾個傭兵在時常受到衛兵關照的這間酒吧大喝特喝，一解今早的悶氣。他們就是找上克莉絲汀麻煩的傭兵與他的幾個伙伴。

在杰羅斯的威脅之下落荒而逃的他們，花了半天來到桑特魯。

這些人之中，只有一個人還記恨著早上發生的事，獨自喝著悶酒。

「可惡，那個魔導士……現在想想還是超火大的！」

「你還在講這個喔？也差不多該死心了吧……」

「秘銀劍都爛了不是？就算搶來也沒辦法用啊，你很笨耶，哈哈哈哈。」

「天曉得。現在想想，說不定是那傢伙嚇唬我們。」

這個人會覺得杰羅斯說謊的理由有二。一是杰羅斯並未證明自己擁有鑑定能力。二是被用劍指著的時候，杰羅斯曾說過「歸還那把劍，從我的面前消失」。

鑑定能力是一種就算自稱擁有，別人也無法加以確認的能力。因為如果不是讓擁有鑑定能力的人鑑定好幾樣東西，根本無法驗證是否真的擁有鑑定能力。

另外，若認定對方拿劍威嚇的行為是想挑起自己的恐懼心，那這一切都是演技的可能性就變高了。這麼一來，擁有鑑定技能本身也就很有可能是謊話。

更重要的是，那模樣看起來就可疑到爆，讓傭兵打一開始就預測錯誤。

「就算是這樣，那傢伙可是真的高手喔？才不是什麼魔導士。」

「嗯……畢竟他在不知不覺間就拔劍了啊。」

「那個不是可以挑戰的人，我還想要命咧……」

「這種事我也知道啦！」

這些傭兵的階級都很低。因為他們沒有認真練等，都只是從旁撿便宜，一路混到現在。在護衛商人的任務之中也是跟在強大傭兵之後，以俗稱寄生的手法來確保自身安全，討伐魔物時也會抓準其他傭兵已經削弱對手實力的時機趁虛而入。

他們認為自己很聰明，卻因為這樣的行為受到批判，無法獲得他人信賴，結果無法提升階級。但他們卻因此怨恨傭兵工會，變得自暴自棄，並不斷偷偷做出這種只能說是招惹他人的勾當。不管走到哪裡，總是少不了這類型的人。

一位原本在吧台喝酒的男人來到他們旁邊。

那是個身穿黑袍的魔導士。

「你們聊的話題很有意思呢，你們難道認為獲得強力武器就會變強嗎？很遺憾，不是這樣的。」

「你說什麼？是想來找我們麻煩嗎！」

「會輸給魔導士的傭兵根本不是我的對手。不過既然你們讓我挺開心的，那我也回報一些有趣的事情給你們吧。」

「啥～？你不也是魔導士嗎？啊你所謂的有趣事情是啥～？」

「在分享之前我想先請教各位，你們想變得比現在更強嗎？如果你們願意回答，我就可以告訴你們變強的方法，變得比別人強的方法。這就是我所謂的有趣的事情。」

傭兵們面面相覷。今天早上遇到的魔導士打扮得就很可疑，但這位魔導士一眼就可以看出他家世不錯，以別種意義上來說反而很可疑。而且這裡明明是室內，他卻用兜帽深深蓋住頭。再加上他只是聽人閑聊就打算分享情報，這真的是太詭異了。

「啊啊！不可以只是說喔，必須付出代價……不過我看你們也沒什麼錢，就用一杯酒算數吧。包括剛剛的笑話在內，用一杯酒就可以當作報酬喔？」

「你這傢伙，只是想從我們身上敲竹杠吧？」

「沒禮貌。我只是因為那是我用不著的東西，才想說要讓給你們的。我也可以讓給別人喔？說不定有人願意出高價購買呢。」

傭兵們再次面面相覷。眼前這個魔導士看起來雖然只是想將用不到的東西塞過來，但目前無法得知那是什麼。不過如果能變得比現在更強，就更有機會賺到大錢。他們行事雖然姑息，但因為總是干些惡質勾當，所以戒心也比一般人高得多。

「你若不讓我們先看看是什麼，我們不會接受喔？」

……理論上是這樣，但因為一群人都喝醉了，就隨便接受這個話題了。

「說得也是，先讓你們看看東西吧。呃～就是這個。」

只見魔導士突然將手伸到空中，從空無一物的地方取出東西。傭兵們全都嚇呆了，但魔導士也不在乎他們，將物品放在桌上。那是一個鑲嵌了黯淡黑色石頭的守護符。

「好了，我讓你們看到東西了喔？輪到你們請我喝酒了。對了，我先聲明，拿走這個東西也沒有意義，我還沒跟你們說這東西的用法。」

「嘖。喂！給這個魔導士一杯麥酒！」

男人大吼，過沒多久之後，一個壯碩的阿婆將麥酒裝進木製啤酒杯裡面，粗魯地放在桌上。不，或許說她靠著一股蠻力砸上來還更貼切一點。

桌子因為這衝擊搖晃了幾下，但啤酒杯裡的麥酒卻一滴也沒灑出來。

「……服務這麼差，真虧她生意做得下去耶？」

「這點我們也覺得很神秘。」

「因為這裡的餐點很好吃。雖然態度真的爛爆了……」

「阿婆現在好像還單身喔？」

「我曾經差點就要被那個阿婆推倒……那時她全身光溜溜……嚇死我了。」

「「「「…………」」」」

充滿同情的眼神全集中在一位男人身上。他現在也嚇得發抖，一副快要哭出來的樣子。那是個體型圓滾滾、眼睛細長、胖到可以登上金氏世界紀錄的阿婆。每走一步路就會壓得地板嘎吱作響，大家都很好奇她體重到底多少。老實說，真不想被這種人襲擊。

「好啦好啦，這傢伙的事情不重要。所以說，這玩意要怎麼用？」

「哎呀～免錢的酒真的特別好喝呢。啊，那個的用法嗎？只要配戴在身上灌注魔力，它就會賦予力量喔？」

「可以試試看嗎？」

「請便？我是不需要這個啦。」

男人用手握緊守護符，注入身上僅有的稀少魔力。

──噗通！

這時他感受到跟過往完全不同的興奮之情，以及一股從體內涌現出來的力量洪流。身體發熱，力量似乎正源源流出。

「哈哈哈，這好贊啊。力量涌現出來啦！」

「真假……我也想要。」

「可以啊，我還有三個左右喔……？」

「「「請把它給我們！」」」

男人們逼近魔導士。被一群男人以超級臉部特寫逼過來實在太嚇人，魔導士臉上帶著抽搐的笑容，將同樣的守護符交給三人。

「哎呀，時間差不多了。我還有工作，先失陪了。」

「喂，這樣就要走了喔？我們還沒向你道謝耶？」

「畢竟是工作啊。要是遲到，又要被上司嘮叨一頓。」

「魔導士也真辛苦……」

「真的。有緣再見了。」

「在那之前我們會先好好大賺一筆的。」

魔導士一邊揮手，一邊離開傭兵那桌。

「……如果還有機會再見就好了。」

魔導士以冷酷的聲音低聲嘀咕，就這樣離開了酒吧。

留下來的男人們一路喧鬧到早上。

◇　◇　◇　◇

離開酒吧的魔導士轉進建築物暗處的小巷之後，與在那兒待命的幾個男人會合。那是幾個看起來就受過專業軍事訓練，十分可疑的危險男子。

「進展如何？」

「嗯，算是順利吧。剩下就是你們的工作囉？」

「那些人也真是悲哀，一定沒想到自己會被當成人體實驗的對象吧。」

「連垃圾都有成果，之後就只要靜待佳音便可。畢竟要判斷那個派不派得上用場，沒經過試驗還是太危險了點。如果順利，就可以開始量產。」

「你也還有工作要做呢……」

男人們浮現疑惑的表情。

「這就要看你們回報的結果如何了。因結果不同，可能得改變方案。」

「我明白，你也有你的目的……所以才願意出手協助我們，不是嗎？」

「現在其實就是一直幫忙，但回報不成比例呢。我也還有很多其他事情要做……既然目的相同，那只能靠你們多加把勁了……」

「……真抱歉。但請你再等等。」

「我會期待的，之後就有勞了～」

魔導士以輕巧的腳步踏入小巷。一副覺得這些男人無所謂的樣子。

幾個男人彼此無言地點點頭，像要抹消自身存在似地消失於黑暗之中。

「他們打算利用我吧，但這點我也一樣。不管事情怎麼發展，只要我能接近目標就無所謂……話雖如此，或許去看看狀況比較好？」

魔導士露出冷酷的笑容靜靜地嘀咕，消失在黑暗之中。

只剩下一片寂靜籠罩著小巷。

◇　◇　◇　◇

當杰羅斯回到桑特魯城時，已經是朝陽升起的時間。

儘管這是個爽朗的早晨，但他卻在別的方向上異常興奮。

『首先要做乾燥機，然後是打谷機，接著是冰箱，啊……還需要那個吧。因為有變魔種，就差設定外型的精靈因子情報……要製造人工卵子的話，是不是用其他種族比較適合？矮人……啤酒桶體型的蘿莉，就道德層面來說要駁回。獸人……可能會變得蠻橫粗暴，所以也駁回。這麼一來，就想要高階精靈的精靈因子情報呢。如果是女性最好，但這點不下去實做也很難斷定。還想要幫忙管理農田的人手呢～唉，總之打造人工生命體這件事就慢慢來，眼下最主要的問題應該是稻米什麼時候才能收成吧。原本是雜草，所以這兩天已經成長了許多，說起來既然一年可以收成七次，應該就不用擔心會斷炊吧？這個國家……拿稻米當主食啦。更重要的是酒，若沒辦法作麴就無法釀酒，也沒辦法做味噌跟醬油。萬能的諸神啊，日本酒，給我日本酒！我將前往芳醇的美酒花園！』

接著他的腦子便陷入一陣歡欣愉悅的狀況。

杰羅斯的腦海已經離不開日本酒了。

「不管怎麼樣，應該以準備好機器為優先。不然就沒辦法釀酒……」

杰羅斯最在乎的是日本酒。看樣子他因為熬夜穿過街道走回來的關係，思想變得有些不正常。看到這樣的他，在城鎮門口待命的年輕衛兵以狐疑的表情看向他。

畢竟這是一個光是外表就夠可疑的大叔，在城鎮大門前一個人不知道碎碎念著什麼，會被懷疑也只是剛好。而且他沒有進門，只是一直在門前晃來晃去，當然會令人起疑。

最後，幾個衛兵朝杰羅斯走來。

「那邊的魔導士，麻煩你來崗哨一下，我們有話要問你。」

「咦？是說……我嗎？」

「除了你之外還有誰？在城鎮前鬼鬼祟祟的可疑人士。」

「就算你說我可疑……畢竟我外表這樣，的確夠可疑吧。」

從旁觀的角度來看的確非常可疑，杰羅斯也有自覺。

「你不僅外表可疑，行為更可疑啊！別囉嗦了，快過來！」

「請、請等一下！讓我解釋，你們一定也會明白的！」

「我就是要你說明，少廢話了，快點走！」

「啊，請問有提供早餐嗎？我連夜從阿哈恩村走到這裡，現在超餓的。希望能提供麵包和蛋……最好是炒蛋。」

「大叔，你臉皮挺厚的嘛？」

杰羅斯就這樣被衛兵帶走，三個小時之後才獲得釋放。

因為對方的誤會，杰羅斯狠狠敲了一頓早餐這點自不在話下。

◇　◇　◇　◇

回家途中，杰羅斯為了賣掉魔石而繞去了魔導具店。

他只來這邊賣過一次魔石，但覺得店面外觀變得格外可愛，讓他一度猶豫要不要進去。這怎麼看都不是個男人該踏進的店家。

「之前的外觀明明充滿著嚇人的詭譎氣氛……到底發生什麼事了？」

之前那彷佛魔女之家的外觀一百八十度大轉變，成了尋常的咖啡廳、甚至更進一步像是女僕咖啡廳那樣的氣氛了。

活像可疑分子的杰羅斯猶豫了一會兒之後，還是打算踏進店家。這時一位身穿有著許多荷葉邊服裝的女僕出來露了臉。

那是以戴著大圓眼鏡為特徵的失禮店員，庫緹。

「啊，你是之前的……誰來著？」

「我應該跟這家店沒有熟到需要報上名號吧？是說這裝潢是怎麼回事……」

「店長一時興起嘍。說什麼『今後應該配合客人的需求改變店鋪的風格』之類的話～」

「發現得太慢了吧，而且改變太多了吧……這不是完全沒有留下之前的影子嗎。」

「多虧飯場土木工程的人賣力工作～」

杰羅斯腦海裏浮現一臉爽朗笑容豎起大拇指的矮人工匠，那古裡的模樣。

他們的動作真的很快，而且會嚴格遵守完工日期。

然後會在隔天，或者甚至當天就轉往更炙手可熱的工地去。

他們是以第一線工地為重的類型。

「之前他們說的下一份工作就是這裡啊……」

「話說，今天有何貴事？我們不收偷來的魔石喔？」

「你還在講這個喔……不管說什麼就是想把我當小偷看嗎？」

「沒錯！這是當然的。」

庫緹就是一個儘管堆滿笑臉，卻會把很不得了的事情推給客人的店員。

要是一直陪她抬杠，真的很有可能會蒙上什麼冤罪，所以杰羅斯決定不理她。

「店長在嗎？」

「在是在，但昨天晚上對帳對到很晚呢～可能在櫃台睡覺吧？」

「……你才剛出來吧，沒有看到店長人嗎？」

「有啊？睡到口水都流出來了呢～」

庫緹用悠哉的口氣說。這對話看起來好像成立，但又有點牛頭不對馬嘴。

「哎，反正只要能收購我手上的魔石就好了，畢竟我一口氣獲得了超乎預料的數量。」

「你又是從哪裡偷來的嗎？快去自首！」

「好，我要跟店長告狀，要快點開除這個店員……」

「店長，有客人喔～」

她彷佛剛剛什麼都沒發生似地轉變態度進入店內，向店長貝拉朵娜報告。

杰羅斯一臉疲憊地入內，就看到店裡也裝潢得也非常可愛，是一種會讓大叔頭痛的裝潢風格。四處裝飾著布娃娃和蕾絲窗簾，甚至還擺設了假花，少女風格的程度非同小可。

以前起碼店裡面的擺設還算普通，現在裡頭則跟外觀一樣滿是粉紅色。

「店長～快起來～有客人啦～……小甜甜～」

「誰！剛剛是誰叫了我的本名？我叫『貝拉朵娜』，這才是我的靈魂之名！」

先不管她為什麼要拿毒草當成自己的名字，但看來店長對自己的本名抱持著自卑感，才用假名經營店鋪。她明明就是個外表看起來像高級娼婦的魔導士，本名卻相當可愛。

就某種意義來說，她的名字跟現在的店面風格很搭。當然這不考慮她本人的外觀。

「店長，有客人來了喔？是之前那個小偷。」

「居然還在說。你也差不多該開除這個店員了吧？」

「哎呀，歡迎光臨。好久不見了呢？看你一陣子沒來了，怎麼了嗎？」

「店長，你那種講話方式就像『夜蝶』耶～？」

「我討厭『夜蝶』這個說法，因為這樣不就是普通的蛾嗎？就像散佈鱗粉那樣散佈香水味的毒婦。我才不會做那種事。」

杰羅斯就算死也不會說他其實心裡想著「不不，你壓根就是『夜蝶』啊」這種話。

畢竟店長正以可怕的表情瞪著他，看來店長的直覺意外地敏銳。

「又要我收購魔石嗎？」

「嗯，但因為之後認識的人也會拿一些過來，所以我希望不要賣到會掉價的程度。」

「你們到底打倒了多少魔物啊？說實話，我還真不敢聽。」

「不好意思，能不能麻煩你調整數量？魔石大多是蠕虫跟蜘蛛的就是了。」

「你去了阿哈恩礦山？這個嘛……蠕虫十五個、蜘蛛二十個的話呢？」

收購數量意外不多。但杰羅斯手中光是魔石就有這些的十倍以上之多，而且這些東西他用不著，老實說很困擾。在數值表示一覽之中不知為何有個「自動回收」的指令，讓他回收了所有打倒的魔物所掉落的魔石。大概是自己在打遊戲時的數值就這樣直接照搬過來用了吧。於是這變成了一種技能，會不斷回收魔石。

杰羅斯本人則是在被衛兵帶去崗哨泡茶的時候才發現這一點。

「好，成交。」

「哎呀？我還沒報價耶。」

「關於這一點我相信你。光是你願意收購我就感恩大德了。」

杰羅斯不在乎錢，他在乎的是酒。他按照魔導具店店長所說的價格售出魔石，換得少許金錢之後，在攤販買了包了餡的炸麵包，踏進路邊的菸草店買了香菸之後打道回府。

「啊～……還需要攜帶式菸灰缸呢。我竟然會忘記最基本的禮儀……太失策了。」

他到現在才發現自己之前都叼著香菸到處跑，還隨手亂丟菸屁股的事實。

大叔忘了基本禮儀。

無法遵守基本禮儀的大人雖然無比差勁，但即便如此他還是不會戒菸。

◇　◇　◇　◇

杰羅斯在鎮上閑晃，走進舊街區，正好來到教會前面。

杰羅斯的家要從教會旁邊那條新鋪設的道路走進去，正好可以看到教會後面的菜園。

────要死了，會死人啊……咳噗！

孩子們正在菜園裡拔曼德拉草。

曼德拉草發出驚人的慘叫。人類這種生物呢，不管碰到多麼誇張的事情都還是有辦法習慣，孩子們和路賽莉絲也已經不會在採收的時候受到精神創傷了。

其實孩子們打一開始似乎就沒事，但因為孩子們也已經厭倦了，不再會因為好玩就去拔草了。

不知為何總覺得身為一個人，好像失去了些什麼寶貴的東西。

「啊，是伯伯！」

「喂～伯伯！」

「沒有伴手禮嗎？」

「給我肉～我要肉～～」

發現杰羅斯之後，孩子們便朝著這邊奔過來。基本上他們的目標是土產。

「有喔，這是伴手禮炸麵包。」

「哇～～伯伯，謝謝你。」

「Thank you，伯伯，我們快吃吧。」

「Danke，伯伯。」

「哈啊哈啊……肉，是肉～～嘿、嘿嘿嘿……這樣又可以撐一段時間了。」

孩子們拿走紙袋之後，活力十足地往教會奔去。雖然不重要，但杰羅斯很疑惑最後那個孩子是從哪裡學到這種話的？難道附近有什麼不太正常的大人嗎？

「喂，你們這樣很沒禮貌！杰羅斯先生，對不起。還有，歡迎回來。」

雖然這番話很平常，卻讓杰羅斯一時之間說不出話來。

「請問怎麼了嗎？」

「不，總覺得有人對自己說『歡迎回來』感覺挺好的……路賽莉絲小姐，我回來了。如果讓你擔心，那還真是對不起。」

這種簡單的寒暄讓長時間獨居的大叔很是開心。

「寒暄是再基本不過的事了，而且擔心認識的人也是理所當然的啊？」

「雖說是理所當然的事情，但有些人聽了就是會特別感動，尤其是像我這樣的獨居者。」

就算在原本的世界，回到家也沒有人會跟杰羅斯說「歡迎回來」。

自己打開漆黑房間的電燈，洗完澡、吃完飯之後看電視，每天過著這樣的生活。在沒動力的時候也有可能整天無所事事，但如果有人能陪伴在自己身邊，也不至於感到這麼孤獨吧。雖然興趣是打線上遊戲，但也是因為孤獨感帶來很大的影響，才會讓玩線上遊戲變成日常生活的一部分。

「如果只是這點小事，我隨時都能跟你搭話啊。」

「這樣會讓我誤會的喔，尤其是像路賽莉絲你這樣的美女跟我說話，我可能會得意忘形呢。」

「你又在胡說了……這是在捉弄我嗎？」

「不不，我可是很認真地這麼說喔。好了，講太多會妨礙到你做事，我也得回去做些準備，就先失陪囉。」

「辛苦你了。若有什麼需要儘管跟我說喔，畢竟我們是鄰居嘛。」

「有事的話，我會不客氣地請求協助的。」

路賽莉絲目送腳步略顯輕盈的杰羅斯背影離去。

雖然杰羅斯比較年長，但擔心他安危的路賽莉絲靜靜地呼了一口安心的氣。

「太好了，還好真的平安歸來了……」

「修女，你那就是戀愛了喔。」

「居然還不肯承認嗎？太冥頑不靈啦～」

「你就乾脆點老實認栽，做了他吧。」

「做了之後要怎麼辦？當肉吃掉嗎？」

不知幾時回到旁邊的孩子們提了許多建議（？）給路賽莉絲。

「你們啊，從哪裡學到這些話的……？之前明明就還很正常啊。」

「附近的大叔。」

「小巷裡的小哥。」

「酒吧老爹。」

「家裏蹲的憂郁大哥和偶爾會跟小哥買東西的瘦弱大叔。」

舊街區真不是個教育環境良好的地方。

從這天起，路賽莉絲就為了孩子們的教育而煩惱著。

就算能改善自身環境，附近的狀況也太糟糕，而且孩子們適應環境的能力實在太過強大了。孩子們的將來會怎樣，就看她今後怎麼教育他們了。