「你回來啦，大哥哥!」

一回到自己家裡，脫掉鞋子，踩著沉重的腳步走過半條走廊，正要走向自己房間，就從左手邊的客廳傳來了這樣一句話。

春雪想也不想，就以含糊的發音答道：

「……我回來……」

說著又繼續往前走，一步，兩步，到第三步時才猛然緊急煞車。

——啥?

剛剛那是什麼?

在春雪的認知裡，有田春雪這個人打從出生至今的十三年又十個月裡，應該都一直是個獨生子。對此他不但沒有不滿，這些年來反而心懷感謝地覺得幸運。但看來自己在無意識中卻越來越寂寞，最後終於引發了幻聽現象?

就算真是這樣，喊大哥哥也太離譜了，而且還是那麼可愛的小女孩嗓音。這該不會是那種叫做「妹妹」之類的都市傳說吧?

正當春雪維持不自然的姿勢懷疑自己，耳裡再次聽見了那不應該存在的聲音。

哼歌的聲音，拖鞋的啪啪聲。還不只這樣，甚至還有一陣很香的氣味傳了過來。這是幻……嗅?有這個詞嗎?

春雪肩膀上的書包沉重地滑落在地，身體轉動一百八十度，踩著僵硬的腳步踏進了客廳。

接著他終於看到了幻覺。

剛走進客廳，就在左手邊，也就是平常都沒有拿來發揮原本用途的廚房裡，看到了幻覺的存在。

年紀大概在十歲左右吧，嬌小而且苗條得讓人嚇一跳的身上，穿著疑似國小制服的白色上衣跟有吊帶的深藍色裙子，裙子上還圍著一件粉紅色的圍裙。偏紅的頭髮在頭的兩邊各綁起一小束下垂的馬尾，線條圓潤的額頭下，長著一張只能以「天真無邪」來形容的臉孔。不知道是不是有點混血，只見她牛奶色的皮膚上長著幾粒細小的雀斑，一對大眼睛也帶著咖啡紅的顏色。如果要用一句話來形容整體的印象——

……是天使?也讀作Angel?

春雪喪失了思考能力，看得出神，而這個小女生則對他投以一瞥，可愛地微微一笑說了：

「我在烤餅乾，大哥哥你等一下喔。」

「……哇!」

春雪到現在才慢半拍地大叫一聲，將自己圓滾滾的身體藏在客廳門後。他搞不清楚狀況，只悄悄探出半張臉窺探。

小女生一副覺得不可思議的模樣歪了歪頭，但隨即又再度朝他微笑，之後轉身察看微波烤箱內的情形。兩束紅髮輕柔擺動，在窗戶射進的冬日陽光照耀下閃閃發光。

事情演變到這裡，春雪才總算做出判斷，認為這個景象不是幻覺。

儘管狀況接近空想甚至妄想，但這個小女生的存在實在太逼真了。也就是說——這肯定是被人在神經連接裝置裡放了惡意程序造成的。一定是這個程序將超高精度的3D模型投影到春雪的視覺，還同步輸入了聲音與嗅覺的虛擬資訊。雖然搞不清楚到底是誰這麼做，又有著什麼目的——

畢竟「妹妹」這種生物怎麼可能真的存在呢?

既然知道是多邊形構成的冒牌貨，也就沒什麼好怕的了。春雪內心暗自得意，一腳踏進廚房，朝臉上帶著微笑，抬頭望著自己的「妹妹」伸出右手。

接著捏住她那長著雀斑的臉頰一拉。

神經連結裝置是對人的意識進行量子連線，純以視覺與聽覺領域而論，目前已經能夠營造出令人區分不出到底是現實還是虛擬的虛擬實境。儘管由於內存容量與CPU效能的限制，頂多只能運算出一個人的影像與聲音。

然而除此之外的感覺，尤其是觸覺的重現性，則由於各種數據很難數據化，研究遲遲沒有進展。要完美重現出「人類的臉頰」這種必須兼顧皮膚材質、肌肉抗力及反射性收縮等多種條件的複雜觸感，根本不可能辦到。所以只要這麼一捏，手上應該就只會傳回一種沒有生命，只像捏到橡皮似的感覺——

「你、你握額喔啊～（你、你做什麼啊～）」

「……嗚、嗚哇啊啊啊?」

春雪嚇得大叫一聲，放開手往後跳開，屁股撞上了冰箱。

感覺非常完美。

一陣柔軟、光滑而且水嫩，也就是只能以完美來形容的「捏十歲小女生臉頰的觸感」——儘管過去從來沒有這種體驗——就在春雪的手指上發生了。

春雪瞪大了雙眼，凝視著因唐突的冒犯而氣得鼓起臉頰的小女生，同時以顫抖的右手繞向脖子上的神經連結裝置，解開固定扣之後一口氣扯了下來。

現在時刻、日曆與應用程序圖標等各種附加現實信息，都從視野中消失無踪。

但小女生沒有消失。

春雪，不好意思喔。

春雪到現在才發現，家用服務器裡留下了母親以這句話起頭的留言，於是再度裝上神經連結裝置，呆呆站著聽完。

【——不好意思喔，親戚的小孩要在我們家寄住兩三天。你應該也認識吧，就是住中野的齋藤，是我的表弟。他突然說要去國外出差，可是我也跟你說過，我從今天起要到上海去，大後天才會回來，所以這孩子要麻煩你多照顧了。有什麼事就發郵件給我吧，以上。】

春雪的母親，有田沙耶在一家總公司位於美國的銀行信貸交易部門上班，每天都要過了凌晨零時才回家，飛去國外出差好幾天的情形更是稀鬆平常，只是不知道這裡頭有幾成是為了工作，又有幾成是跟她正在交往的男人度假。春雪甚至覺得，要不是七年前離婚的原因出在父親花心，家庭法院恐怕不太可能將親權判給她。

也因此，春雪從讀國小的時候，就頻繁地被母親寄在同一棟大樓裡下兩樓的倉嶋家——也就是千百合的家裡。

千百合的母親跟父親每次都和顏悅色地迎接他，如果他們曾經有任何一次表現出嫌麻煩的模樣，相信自己一定會覺得非常難堪而且無處容身，也許早就成了一個脾氣比現在別扭十倍的小孩。

春雪腦中一邊轉著這些念頭，一邊看著忙著在廚房裡跑來跑去的齋藤家小孩。

烤箱的定時器發出輕快的聲響，小女生立刻拉開烤箱的門，端出一個金屬托盤。充滿甜味的香氣立刻變得更為濃厚，看樣子這陣芳香就是來自這些餅乾。

小女生拿著夾子，小心翼翼地將十幾個餅乾夾到一個鋪上調理紙的大盤子上，這才鬆了一口氣。

她兩只手端著盤子，滴溜溜轉過身來，以視線往上的眼神抬頭看著春雪。

「這個……對不起，我擅自用了你們家廚房。我是想說春雪哥哥回到家裡，肚子應該也餓了……所以才……」

春雪心想，她說話的聲音可比先前小得多了。

對喔，這孩子也很擔心，擔心寄住的親戚家「大哥哥」會擺出一臉嫌麻煩的表情。雖說面對沒見過的女生，但現在的情形可不容年長的我畏畏縮縮啊。

春雪一邊感受著胸口一陣連他自己都覺得太多愁善感的隱隱作痛感，一邊擠出最和善的笑容說：

「謝……謝謝妳，我肚子都快餓扁了。」

這一來，小女生也像冰塊融化似地嘻嘻一笑：

「你、你好，我叫齋藤朋子，讀國小五年級。我們已經好幾年沒有見面了，我想大哥哥可能已經不記得我……我跟大哥哥算是遠房表兄妹。這個……小女子不才，還請多多指教。」

被一個小女生捧著盤子對自己鞠躬，春雪立刻受到脈搏急速上升與汗腺全開的現象侵襲。

但他立刻想起了不久前才下定的決心，勉力響應出一段勉強可以聽懂的招呼。

「妳好，我、我叫……有田春雪。我、我才要請妳，多多多多指教，齋藤小姐。」

被對方立刻微笑著回以一句：「叫我朋子就好了!」春雪只覺眼前一黑，拚命拉回差點遠去的思緒。

對於住中野的齋藤家，老實說他只依稀記得好像有這麼個親戚，想來一般人對表姨丈這種遠房親戚也只會有這樣的印象吧。

「……妳、妳也是獨生女?」

春雪這麼一問，朋子就點了點頭。

「我的家人就只有爸爸。他突然要去出差，我說我可以一個人看家，可是爸爸就是會擔心。前不久他才從學校一路送我來這裡，然後就直接跑去成田機場了。」

朋子一邊將裝餅乾的盤子放到桌上一邊這麼回答，春雪聽了以後忍不住想問個清楚：

「啊，那，妳沒見到家母囉?」

「是。我只收下了大哥哥家裡的臨時通行碼。」

這可是莫大的幸運。憑自己母親的個性，絕對會毫不猶豫地露出純度百分百的嫌麻煩表情給朋子看。

——可是。

咦，這麼說來，該不會說，接下來這三天我都要跟她兩個人獨處了?

不對不對不對，你這個笨蛋，有什麼好慌的，對方還只是個國小五年級的小孩子啊，跟我足足差了兩歲……兩歲……足足?差兩歲可以說是足足嗎?

看樣子朋子也沒發現春雪心中突然產生的焦躁，再次微笑說著：「請等餅乾涼一點再吃喔。」說完就折了回去。她利落地洗好洗碗槽裡的碗盤，同時煮開一壺水，短短幾分鐘之後就帶著盛著茶的托盤一起回到客廳。她顯然已經比春雪還要更適應這個家的廚房了。

春雪心想，女生這種生物實在是很厲害，但隨即又搖了搖頭。是小孩是小孩，對方還只是個小孩。

然而餅乾卻好吃得幾乎可以直接拿去店裡賣了。

春雪轉眼之間就解決了九片尺寸有點大的餅乾，心想真不知道有幾年沒有吃到別人親手做的糕點，啜了一口朋子泡的紅茶。

而在桌子的對面，則可以看到這個紅髮的表妹正一臉認真地吹著茶。她一舉手一投足的模樣，都是那麼純樸而且惹人怜愛，光看都會覺得心裡溫暖起來。

「……我吃飽了。這個……很、很好吃。」

好不容易擠出正常的聲調這麼說完，朋子就鬆了口氣似地露出滿臉笑容：

「真的嗎?那太好了!大哥哥什麼都不說，害我好擔心耶。」

「對、對不起，我吃得太投入了……」

「真的是這樣呢。」

說完呵呵笑了幾聲，微微起身朝春雪伸出手，拿掉黏在他臉頰上的餅乾屑。

接著丟進嘴裡，又朝他笑了笑。

春雪覺得自己腦海中似乎發生了一種一箭穿心似的奇怪音效，趕忙擦了擦嘴邊。

「這、這個，那個，呃……對、對了，接下來我們要怎麼辦。要玩些游、游、遊戲嗎?我家多得是遊戲，從四十年前出的到現在都有，堆得……」

說到這裡，春雪才想起這些遊戲大部分都是血腥暴力的人間煉獄型遊戲。

但所幸朋子微笑著搖了搖頭。

「不好意思，我不太玩遊戲。因為我對完全沉潛不太拿手……」

「這、這樣啊。」

聽完轉動目光看去，春雪這才發現她那連襯衫最上面的扣子都有扣好的細嫩頸子上，並不存在已經成為現代生活必需品的量子連線裝置。

這年頭確實有不少家庭，不願意讓小孩從小學生的階段就常態性裝上神經連結裝置，因為廣大無際的全球網絡可說是犯罪的溫床。就算有提供家長過濾功能，還是很難百分之百阻隔所有有害信息。

如果一個人平常都只在學校上課時才使用視聽覺模式，也不難理解會對完全截斷現實世界中五感的完全沉潛覺得害怕的心情。春雪拚命思索不能玩遊戲的話該怎麼辦才好，這時視線才總算停在客廳牆上的大尺寸超薄電視上，於是輕輕指了指屏幕問道：

「那……那要用那玩意看個電影嗎?以前的2D影片裡也有一些挺好看的作品。」

但朋子還是輕輕搖搖頭，害羞地說了：

「我是想說……我們要不要聊聊天?跟我說些大哥哥國中的事情嘛。」

說著站了起來，踩著小小的腳步繞過桌子，坐到了春雪身旁。

一陣牛奶似的甜香刺激鼻腔，觸動了春雪長年培養出來的反女生力場，讓他反射性地想要跳開。椅子立刻被帶歪，眼看就要往左邊倒去，春雪立刻雙手亂擺一通，這才勉勉強強保持住平衡。

朋子盯著喀噠兩聲讓椅子回到原位的春雪打量了好一陣子，才輕輕一笑說了：

「沒想到大哥哥還挺可愛的。」

——嗚哇

春雪聽著泡沫從自己嘴裡冒出的聲響，讓身體在浴缸裡坐得更深了。

由於母親的堅持，有田家的浴室格外寬廣，浴缸也非常大，就連春雪這麼龐大的身軀，也能在裡頭自在地伸展開來。他從鼻子大大吸了一口有著香皂氣味的水氣，在肺裡囤積了一會兒，再細而緩地呼出。

儘管口才已經不是一個差字可以形容，但長年沒有像今天這樣連續使用聲帶這麼久，讓他喉嚨都隱隱作痛。隔著朋子作的咖哩飯晚餐，算來竟然整整聊了四個小時，甚至讓春雪佩服地覺得，真虧自己的日常生活裡會有那麼多東西可以講。

結果春雪從梅鄉國中的各種制度，與兩位從小就認識的朋友之間所發生過的各種插曲，甚至連自己「最重要的人」，也就是那位黑衣學姐的種種，都一五一十地說了出來。唯一沒有提到的，就是一直持續到幾個月前的霸凌事件——以及跟「那個世界」有關的事情。

而這些怎麼想都不覺得有趣的話題，朋子卻聽得十分認真，有時還笑出聲來。

春雪心想有妹妹大概就是這種感覺，並深深地在心裡感受了一番。

同時心中總有一抹覺得不對勁的感覺揮之不去，讓他厭惡這樣的自己。

他總覺得事情實在太——太如意了。有一天放學回家，就突然多了個妹妹，不但會為自己烤餅乾，煮咖哩，還說「想跟大哥哥多聊聊」，甚至還要跟她獨處三天。

春雪的成長歷程沒有那麼順遂，沒辦法把這種情形當成天上掉下來的稀有事件來享受。

然而就算這件事背後有內幕，又到底會是誰，為了什麼目的而安排的呢?自己又該怎麼查證才好?

春雪想了一會兒，上半身探出水面，從一旁的三角置物架上拿起鋁銀色的神經連結裝置。

儘管裝置本身有做過生活防水處理，但春雪還是仔細擦乾脖子上的水滴，再從後頸戴上裝置。U字形的兩端部分輕輕往內側一甩，牢牢固定在頸子上。

一打開電源，開機標示就在眼前亮起，經過二十秒左右的大腦連線檢查後，虛擬桌面就在視野中層開。春雪迅速動起右手手指，打開有田家家用服務器的窗口。

準備從數據儲存區點進全家相簿之際，春雪不禁有些猶豫。這幾年來都沒有全家一起拍過照片，但這裡頭應該有一大堆春雪吃得胖嘟嘟之前——也就是父親跟母親感情還很和睦時的照片。這種東西他死也不想看。

春雪回到上一層，打開了與自家服務器連線的網絡芳鄰。

啪啪幾聲響起，好幾個網絡入口以立體方式展開。這是個完全由有田家親戚所構成的家族網絡。當然就算是家族網絡，也不能擅自翻閱服務器內的資料，但還是可以在裡頭留下留言，或是察看一些有對親戚公開的行程等數據。

然而裡頭卻找不到「住中野的齋藤家」用的入口。一般家庭都會在首頁放上兼作近況報告用的全家福照片，所以他本來是想去查看齋藤家的照片，但看來家族網絡終究只有連到母親娘家跟幾個兄弟姐妹或叔叔伯伯家的網站，母親的表弟這種遠房親戚就沒有涵蓋進來了。

春雪先將目光從虛擬桌面上移開，傾聽浴室門後的聲響。依稀可以聽見客廳裡超薄型電視的聲音，看來朋子還在看適合合家觀賞的綜藝節目。自己是在朋子的堅掙下先洗的，不趕快讓出浴室就太過意不去了，更別說泡得這麼久的理由，竟然還是因為懷疑她其實不是自己的遠房表妹。

春雪再次瞪視著桌面，打開浮在正中央的入口——也就是母親娘家的家族網站了

春雪也不管那張以山形縣農村為背景所拍的田園風全家福照片，點選了通往網站內部的入口。這時當然會有認證窗口出現，擋住春雪的去路。

春雪在窗口上輸入了家族分配給母親的ID跟密碼。這種連線會在對方網站上留下記錄，要是對方向母親詢問登入的理由，春雪盜用母親ID的事就會拆穿，肯定會被痛罵一頓，不過他怎麼想都不覺得，經營櫻桃農園的外公跟外婆會去檢查自家網站的登入記錄。

然而冒險的事情當然還是越快做完越好。春雪以最快的速度潛入母親娘家的家族網站，打開了相簿資料夾。

儘管累積了幾十年的龐大照片量讓他看了就煩，但春雪懂得以拍攝時期及人數等條件加以過濾，抽出自己要的數據。如果自己依稀的記憶沒有錯誤，大約五年的外公的壽宴上就來了相當多有田家的親戚。印象中他跟「住中野的齋藤家」也在那個時候打過招呼。既然如此，當時五歲左右的朋子應該也有在場。

搜尋很快就有了結果，好幾張照片的縮圖重疊顯示在視野之中。

春雪用指頭接二連三地彈開不對的照片。

不是這張，也不是這張……啊，應該快到了，大概就是下一張。

「大哥哥☆」

突然從右側傳來一句歌唱般的說話聲音，讓春雪反射性地轉過頭去。

舉在空中的右手手指當場僵住。

不知不覺間，浴室的門已經拉開一條門縫，可以看到朋子露出臉跟右肩站在門後。

視線從她那以毛巾綁住的咖啡紅頭髮往下，一路掃過略帶靦腆的臉孔，纖細的脖子與肩膀細嫩的肌膚——

「妳……妳、妳……」

朋子朝著嘴巴做起高速開閉運動的春雪，露出了淡櫻花色的笑容。

「大哥哥，我可以一起洗嗎?」

「妳……等……怎……」

「誰叫大哥哥泡那麼久，人家都等到不耐煩了啦。」

說著嘻嘻笑了幾聲，朋子也不等春雪回答，就踩著小小的腳步跑進了浴室。春雪趕忙猛力讓身體往水裡一沉，用力閉緊雙眼大喊：

「對、對不起我馬馬馬上出去!我我我我馬上就出去妳再等我一下就好!」

「沒關係啦，我們是遠房表兄妹呀。」

怎麼可能會沒關係啦啊啊——!

儘管春雪在腦內這麼嘶吼，但身上所配備的生體光學式觀測裝置——也就是他的眼球——卻背叛了主人的命令，擅自微微睜開眼睛。踩在象牙磁磚上的一雙小小的赤腳衝進視野之中，看得春雪停止呼吸。

雙眼的焦點自動往上移動。她的小腿肚苗條得讓人驚訝，劃出了流暢的曲線。又小又圓的膝蓋底下，連接了修長的小腿。

而她的雙腿就在快到根部的地方，被一條粉紅色的浴巾恰好遮住，讓春雪一瞬間覺得凝事。儘管自責自己真是個色狼，竟然還嫌浴巾礙事，但視線還是繼續往上走。浴巾緊緊裹住了她幾乎沒有曲線的軀幹，就在隨時都有可能鬆開的浴巾綁合處上面不遠的地方，細嫩的肌膚上還可以看出纖細的鎖骨。

「可……可是大哥哥也不要一直盯著人家看啦。」

最後視線來到了她害羞得低下頭去，長著雀斑的臉上。

春雪拿顯示在視野左側的一張五年前有田家族大合照，跟她的臉孔仔細比對。

前排排著包括自己在內的一大群小朋友，到現在已經根本認不出誰是誰，所幸這個時代的照片已經採用了鑲嵌數字數據的技術。

隨著目光焦點不斷移動，小朋友們的名字也隨即在身上浮現又消失。

而春雪要找的名字就在第六個人身上出現了——「齋藤朋子」。

目光凝視之下，符合條件的小朋友臉孔就自動拉近，放大到跟眼前的朋子同樣大小。

當時她才五歲，俗話說女大十八變。有五年的時間，已經夠讓她的長相變成現在這樣……

才怪。

春雪深深吸一口氣，隔了一會兒才慢慢吐出。

接著就朝著這名睜大了眼睛，自稱是自己遠房表妹的女生，以悲哀的微笑喊了她的名字：

「朋子……」

「什麼事啊，大哥哥?」

「……妳，是個『新來的超頻連線者』對吧?」

她的反應實時而且貼切。

朋子那惹人怜愛的臉孔一瞬間張大了嘴，露出不加掩飾的驚訝表情。

她的臉頰脹得通紅，雖然多半不是因為感到羞恥，右眼眼角還連連抽動。

然而值得佩服的是，這位年齡應該確實只有十歲左右的少女，卻還以可愛的嗓音歪了歪頭說道：

「咦?大哥哥你在說什麼呀?超……頻什麼來著的?那是什麼?」

「晒痕。」

春雪小聲地這麼回答。

「咦?」

「妳脖子上有很明顯的晒痕，幾乎跟我一樣明顯。如果不是從出生就常態性配戴，實在很難晒得這麼明顯……就是配戴神經連結裝置造成的。」

朋子……想來多半是假名的這位少女，雙手立刻遮住了脖子。春雪則接了句「而且」兩字繼續說下去：

「祖父家的家用服務器上，還留著五年前的照片，上面就有拍到齋藤朋子……這麼說是很失禮，不過妳比她本人可愛十倍。」

小女生的臉孔再度連連抽動，露出了極為複雜的表情。

沒過多久，百變的表情終於停住，變成了一種與先前純樸的神情離了有一光年之遠的不爽模樣。

「啐。」

她雙手插在圍著浴巾的腰上，用力啐了一聲。

「虧我還翻過這個家的相簿，確定沒有她的照片，沒想到你竟然連外公家的網絡都去挖，疑心病也太重了啦。」

聽到她突然切換成另一種語氣，固然讓春雪驚訝得瞪大眼睛，但仍然好不容易回了嘴：

「是……是妳太亂來了啦。我想妳應該是偽造了齋藤家寄給我媽的郵件，可是如果我媽跑去跟對方查證，妳打算怎麼辦啊?」

「從妳媽的神經連結裝置發往齋藤家的郵件跟通訊，全都會被攔截到我這邊。虧我還準備了整整三天呢。」

「這……還真是辛苦妳了。」

春雪抓著浴缸邊緣，忍不住發出了覺得不敢領教的聲音。

要在別人的神經連結裝置上放進病毒，唯一的手段就是拿傳輸線直接插上裝置。看來這名少女應該是查清楚了春雪母親的動向，到她常去的健身房，趁她將神經連結裝置放在更衣室置物櫃的時候動了手腳。

自己的骨肉至親被人這樣對付，心裡自然不會舒服，但春雪最先產生的感想卻是佩服。這世上有不少自稱是黑客或是巫師的連線裝置使用者，卻沒有幾個人膽敢從安全的家裡走出來，在現實世界中進行「社交工程」——也就是偽裝成別人，不靠網絡就直接突破安全防護的終極入侵。

大概是聽出了春雪的語調中蘊含了讚賞的意味，少女臉上露出得意的笑容。

春雪抬頭看著她的表情，說出接下來的推測：

「……妳這麼大費周章，應該是想拿我當跳板去駭『她』，不過妳太天真了。她那麼有見識，第一眼看到妳的瞬間就會發現妳是冒牌貨了，才不會像我花了五個小時才發現……當然以超頻連線者的身分光明正大去挑戰又贏不了她，這我也不是不懂啦……畢竟對方可是大名鼎鼎的『Black Lotus』……」

春雪一邊期盼她可以快點出去，一邊斷斷續續地說完。

這一瞬間。

小女生散發出來的氣息再度發生劇變。

她雙眼猛然發出跟頭髮同樣的紅色光輝，有光澤的嘴唇歪成了不搭調的ヘ字形，雙唇的縫隙間微微露出純白的牙齒。

她以這種只能用傲慢不遜來形容的表情低頭看著春雪，放低嗓子說道：

「——喂，你這小子剛剛說什麼鬼話?」

「……咦?就、就是說……光明正大去挑戰也……」

「贏不了她?我會贏不了她?所以我才要這樣偷偷摸摸在現實世界裡搞這麼麻煩的滲透工作?」

——不這樣嗎?

就在春雪以視線這麼發問的同時，小女生的右手從頭上扯下了毛巾幫用力甩在地板上，食指用力朝著春雪一指。

一陣水氣之中，春雪產生了一種錯覺，覺得她那頭接近火紅色的紅髮豎了起來。隨著甩頭的動作，她的一頭短髮就像火焰似地搖擺，粗著嗓子撂下狠話：

「夠了，有夠麻煩，不搞了，我就直接用實力逼你就範。敢看不起我『Scarlet Rain』我會要你付出慘痛的代價，我這就去拿神經連結裝置，給我乖乖等著!」

她收回右手食指的同時還朝下挺出拇指，做出往旁一拉的動作之後，才猛然轉過身去。

接著踏出一步的右腳，就踩在先前自己扔下的毛巾上面，當場滑了一跤。

「呀啊!」

一聲尖叫。春雪抬頭看著這一跤滑得像是後空翻一樣漂亮的她，也跟著大叫一聲：

「哇?」

春雪反射性地張開雙手，在小女生一頭撞上浴缸邊緣之前接住了她。然而春雪踩在水裡的腳卻突然打滑，讓他也跟著往後一翻。

嘩啦。

伴隨巨大的聲響，浴室裡濺起高高的水柱，一條大浴巾在一旁飛舞。

春雪的頭輕輕在身後的牆壁上撞了一下，他用力閉緊眼睛，等疼痛略微消退才微微睜開眼睛察看狀況。

自己跌坐在寬廣的浴缸裡。

胖嘟嘟的肚子成了小女生的肉墊讓她坐在上頭，而自己的雙手摟住了她纖細的身軀。

雙方都是全裸。

「嗚、嗚哇啊啊啊啊!」

春雪大叫一聲。

「嗚嘎啊————!」

卻被她的尖叫聲蓋了過去。她先胡亂掙扎了一會兒，最後靠著在春雪肚子上猛力一蹬的反作用力，一口氣逃出了浴缸。接著撿起地板上的浴巾，同時以超高速衝到脫衣間，再次探出臉來說了句：

「……我宰了你。」

春雪聽著她重重的腳步聲往客廳遠去，茫然地想著。

——我看到，還摸到了。

——不對，重點是她多半是六王軍團之一所派來的奸細。而從她的言行舉止來判斷，等會兒她應該會來找自己對戰。

那應該拿下神經連結裝置來回避嗎?然而如果她真的是今後得正式對上的敵人，總是越快得到她的情報越好。自己才剛升上4級，就算輸個一次，也拙不了太多點數，而且一一對方還是個小孩子，春雪不覺得自己會那麼容易就打輸。

儘管有八成左右的思考回路都還處在極度的混亂當中，但春雪仍然用剩下的兩成效能想到這裡，接著從腦中喚醒了先前她說出來的名號。

「Scarlet Rain」。這個名字自己應該沒有聽過。在色相環上應該屬於「象徵遠攻的紅色」屬性，但要就此認定她屬於紅色軍團，則未免言之過早。這個問題只要對戰看看就會知道答案，不過他還是希望可以多獲得一點相關信息。

離她裝上神經連結裝置，啟動操作系統，完成量子連線檢查，應該還有幾十秒的空檔。春雪就這麼癱坐在浴缸裡，以語音指令說道：

「語音呼叫指令，編號零一。」

這句話才剛說完，眼前立刻出現一個對話框，顯示【是否確定與登錄地址零一號聯絡人進行語音通訊?】，春雪立刻點選YES。

鈴聲響了兩聲，對方接通了通訊。

『是我。怎麼啦?春雪，這種時間打來找我。』

這個嗓音有如絲綢般柔順，又有如音樂般富有抑揚頓挫，而背景還可以聽見潑水的聲音。

啊啊，不知道學姐是不是也在洗澡……春雪瞬間浮現出這個念頭，同時對通話對象——身為最強超頻連線者之一的黑之王「Black Lotus」，也就是黑雪公主說道：

「不好意思，這麼晚了還打擾學姐。我有事情想請教學姐……」

『哦?什麼事?』

「就是，學姐有聽過一個叫做『Scarlet Rain』的超頻連線者嗎?」

他得到的答案是一陣有點長的沉默。

「……請、請問，學姐妳怎麼了?」

『……沒有，不好意思。你應該不是在開玩笑吧?』

「哪有什麼開玩笑……我當然是因為真的不知道才問。都這麼晚了，我才不會打什麼惡作劇電話。」

『是嗎?唔，這應該算是我的疏忽，跟你說起的時候都只用通稱，沒有告訴你名字。不過「Silver Crow」，你也太不用功了點吧?』

「咦……?這話怎麼說……」

歪著頭的春雪，同時聽到一陣猛然從走廊跑過來的腳步聲，與黑雪公主爽朗的聲音。

『——「Scarlet Rain」，外號為「不動要塞（Immobile Fortress）」、「血腥風暴（Bloody Storm）」……那不就是第二代紅之王本人嗎?』

……妳說什麼?

春雪聽得目瞪口呆，思考當場停住。

緊接著紅髮的小女生就猛力拉開浴室門，再次出現在春雪眼前。

大概是氣得顧不了那麼多，全身上下只穿著可愛的內衣褲。然而她似乎已經不想遮掩，昂然挺起白皙的身體，雙手環抱在胸前。

春雪反射性地就要撇開視線，但發現她身上除了內衣褲以外唯一的配件，忍不住細細訂量起來。那個物體纏在她細嫩的脖子上，發出光澤動人而且通透的火紅色光輝。

小女生露出看得見一口白牙的凶惡笑容，以惹人怜愛卻又充滿威壓感的嗓音大喊：

「超頻連線!」

啪!

一聲早已聽慣，但每次聽到仍然不免戰慄的聲響，響徹了整個世界。

轉眼間五感都被截斷，一片漆黑之中，只見一排熊熊燃燒的文字寫著【HERE COMES A NEW CHALLENGER!!】，緊接著視野又恢復正常。

然而眼前的光景已經不是設有象牙化妝台的自家浴室，而是一個怎麼看都覺得像是鑿空了大樓中好幾層樓的廣大平面空間。

春雪已經完全沉潛到了神經連結裝置內的思考加速對戰格闘遊戲，「BRAIN BURST」所創造出的虛擬世界之中。周圍的世界是由根據設置在日本全國的治安監視攝影機網（Social Camera Net）所拍到的影像，所重新建構出來的虛擬實境，正式名稱叫做「對戰空間」。

然而包括春雪的住家在內，一般民宅內基本上都不存在公共攝影機，所以會像這樣以推測方式彌補——也就是由軟件自行杜撰出內部構造。看樣子這次整棟大樓都被還原到了建設中的階段，只看到許多鋼筋穿出了只打上水泥的樓層。

就在這個單調的空間裡，春雪跟小女生大約有半秒左右，都以原來的模樣面對彼此。

但兩者的身體顏色以及形狀都隨即開始有了改變，轉變成為各自用來對戰的分身「對戰虛擬角色」。

春雪圓滾滾的四肢從未端開始裹上一層銀色的光輝，同時也收得越來越細，最後出現的是一對裹在白銀裝甲之中的機械手臂。變化轉眼間就延伸到了軀幹，腰圍一口氣縮到一半以下。就在這極細金屬身軀完成的同時，連頭部也被白色的光芒吞沒，套在一個有著光滑圓形鏡面的頭盔裡。

春雪一邊意識著自己變身成對戰虛擬角色「Silver Crow」的過程，一邊凝視著她那站在幾公尺外的身影。

如人偶般嬌小的手腳突然裹在一層朱紅色的光輝中。隨著光環往上攀升，全身都逐一裹上一層通透的紅寶石色裝甲，平坦的腹部跟胸部，也裹在由暗灰與紅寶石這兩種顏色為基調的半透明裝甲之中，最後再發出一瞬間的閃光，出現了看起來像是人造人的頭部。

面罩上只存在著形狀渾圓的雙眼，仿瀏海造型的裝甲兩側還往外伸出呈馬尾造型的天線，

雙馬尾輕快地跳動，雙眼亮起了鮮紅色的光芒。

——這就是「紅之王」?

春雪呆呆站在原地，低頭打量著站在幾公尺外的對戰虛擬角色。

她的個子很小，身高只有一百三十公分左右。身上看起來像是武裝的，就只有一把掛在右腰上，看似玩具的手槍。

「……我、我說啊。」

春雪無意識中開了口，加上金屬質感特效的嗓音從鏡面頭盔下傳出。

「妳，真的是……」

妳真的是加速世界中僅有七個人的9級超頻連線者，也是率領巨大軍團的最強支配者「純色六王」之中的其中一個?

就在春雪想問出這個問題的當下。

嬌小的少女型虛擬角色身後的空間突然開始扭曲變形。

四個發出火紅色光輝，棱角分明的方塊從虛空中涌現而出，裹住了少女的雙手雙腳，接著更有厚重的裝甲板從左右纏繞過來，完全遮住了嬌小的身軀。

「這……」

春雪張大了嘴，呆呆地抬頭望著質量一口氣擴張到自己數倍之多的火紅虛擬角色。

然而追加裝甲的增加卻還沒有結束。

隨著一聲聲沉重的低音，無數巨大的六角柱、圓筒或鋼板狀物體接二連三從後出現，逐一連接到本體上。高度在轉眼之間就直逼天花板，全長也像是要追上急忙後退的Silver Crow似的，迅速超過兩公尺、三公尺……

幾秒鐘後。

當四周恢復了寂靜，屹立在春雪眼前的，已經是一個有如戰車一般，甚至可以說是要塞的存在。

存在於原來手臂延長線上的兩挺又長又粗的砲身慢慢舉起，各個散熱孔嗤嗤作響的噴出陣陣白煙。

就在整個武裝貨櫃集合體的正中央，可以看到微微露出的兩只紅色眼睛發出了亮光。

「…………不會吧…………」

就在春雪這麼自言自語的同時，以熊熊燃燒的字體顯示的【FIGHT!!】字樣發出光芒並炸了開來。

還是先跑再說吧!

春雪第一個念頭就是想跑，差點就要轉身朝後猛衝，最後總算打消了這個主意。

敵人的屬性是「象徵遠攻的紅色」。這個巨大要塞型對戰角色，怎麼看都覺得遠距離攻擊一定強得跟鬼一樣。除了左右兩門主砲，雙肩上的貨櫃多半是飛彈發射筒，往前方伸出的短砲身武裝則應該是機關砲類的武器。面對這樣的對手還主動拉開距離，簡直是愚蠢透頂。

春雪做出這個判斷，卯足了有限的膽識正面展開對峙，對此，要塞虛擬角色「Scarlet Rain」則以火紅色的視線照射在他身上。

「……哼?你不跑啊?還挺有膽子的嘛。」

紅之王以帶著金屬質感聲響的可愛嗓音放話。

「我、我是嚇得腿軟動不了。」

春雪以沒出息的聲音這麼回答，視線同時拚命在紅之王身上各處掃過。

一般遊戲裡遇到這種巨大而且配備重武裝的敵方頭目，攻略法都一律是想辦法從死角逼近，針對弱點加以擊破。不用說是從正面硬衝了，就連左右多半也逃不出那對可動式主砲的涵蓋範圍。這麼看來，唯一的死角大概就是正後方了。只要全力衝刺繞到對方背上，應該就有辦法應付。

也不知道是不是猜出了春雪的這種念頭，Scarlet Rain輕笑了幾聲：

「說得這麼可愛☆可是啊，你應該沒有忘記吧?」

「咦?沒有忘記……什麼?」

「沒有忘記我說過……」

右側主砲忽然一動，企圖對準春雪。

「——我要宰了你這個變態!」

「那是不可抗力啊啊啊啊!」

大喊著反駁的同時，春雪猛力往地上踹，以快如閃電的動作衝向敵人左側，再做個銳角變向朝她身後前進。

考慮到Scarlet Rain龐大的身軀，她轉身跟上春雪的回旋速度可說是快得驚人，但終究沒有快到可以跟得上Silver Crow這種專往速度發展的單一能力專精型對戰虛擬角色。

「而且明明就是妳自己先跑進浴室來的啊啊啊!」

春雪又喊了一句，同時繞過相當大的角度，朝著終於看到的敵人背部一口氣衝了過去。

他所料不錯，對方背上只有巨大的散熱片跟推進器之類的裝置，看不到任何武裝。春雪看準了裝甲應該最薄弱的飛彈發射器與散熱片連接處，舉起右拳——

……推進器?

就在想到這裡的同時，四個並排的黑色噴射孔一齊噴出了強烈的火焰。

「哇，好燙——」

被火焰裹住的瞬間，全身產生了猛烈的滾燙感，讓春雪發出慘叫。自己那條顯示在視野左上方的HP計量表迅速消減。

然而春雪並沒有停止衝鋒。

產生的傷害量並沒有大到值得害怕，具有金屬色屬性的Silver Crow對於火焰攻擊有著很高的抗性。

「火焰對我Silver Crow不管用!」

——成功了。

春雪心想這下成了，就要朝背面裝甲的接縫處揮出灌注全身力道的一拳。

「小子你太天真啦!」

就在這幾乎讓人可以看見臉上寫著哼哈哈哈哈幾個大字的呼喝聲發出的同時，「Scarlet Rain」雙肩上的飛彈發射器護蓋猛然掀開。

看到無數的小型飛彈從裡頭大舉竄出，春雪驚訝得瞪大了眼睛。

等……這裡，是建築物……裡面……

緊接著，水泥天花板、地板以及所有鋼筋，全都裹在火紅色玫瑰似的爆炸之中。

正當春雪拚死躲過筆直朝自己飛來的一發飛彈，頭上的水泥已經產生網狀裂痕，隨即開始崩塌。

「不會吧——!」

春雪大叫著猛力衝刺，現在已經顧不得什麼不要跟敵人拉開距離的原則了油這裡是二十三樓，離地表有一大段距離，要是被捲進崩塌之中，HP大概一瞬間就會歸零。

這棟曾經是春雪住處的公寓大樓還只蓋好地板跟支柱，所以在距離十幾公尺外的地方，就可以看到通往屋外的空間。春雪在崩塌的地板上左右跳來跳去，一邊以拳頭跟頭錘粉碎崩落的水泥，一邊瞥向自己體力計量表下面的必殺技計量表。

雖然所受的損傷跟對敵人造成的損傷都還不多，但應該是場地破壞點數有加算上去吧，計量表已經有兩成左右的長度發出綠色的光芒。這麼說來——

可以飛了!

春雪深深吸一口氣，雙肩灌注力道。

折疊在背上的金屬翼片發出唰的清脆聲響張了開來。

隨著翼片開始高頻震動，春雪的衝刺速度也不斷增加。

「喝喔喔喔喔——!」

春雪大喊一聲，頭前腳後地朝著逼近眼前的灰色天空毅然衝了出去。

他的住處位於整棟大樓之中相當高的樓層，所以從建築物內衝出來的那一瞬間，眼前立刻出現一片從高圓寺連往新宿的街景。

這片超廣角的景色堪稱絕景，然而所有建築物都跟自己家一樣，換成了鋼筋從水泥中穿出的半成品，看上去非常煞風景。看來這裡應該是「風化」屬性的場地，記得屬性內容是容易損壞、灰塵很多，有時還會突然有強風……

春雪一邊想著這些念頭，一邊放慢金屬翼片的加速，在空中懸停。

朝必殺技計量表一瞥，看到還剩下少許長度，照這樣看來，應該可以連續飛個三分鐘。

轉過身去一看——

巨大的高樓大廈正好就在這時從中折為兩半，令人慘不忍睹地倒塌。

「唉唉……我家就這麼完了……」

他忍不住自言自語。當然這只是由系統產生的多邊形數據數據，但他還是第一次看到自己家在「對戰」過程中遭到破壞。

「真是的，有夠亂來。」

春雪搖了搖戴著頭盔的頭，低頭看著化為大堆斷垣一殘壁的大樓。看樣子紅之王已經被自己造成的崩塌給牽連進去，看不到人在哪兒。照這情形看來，就算是要塞型虛擬角色，大概也承受不住。

就在春雪歪著頭納悶她到底想做什麼，並且開始下降的時候。

春雪發現一件事，不禁全身戰慄。

Scarlet Rain的體力計量表——沒有減少。說得精確一點，是有減少3%左右，但這樣算不上是受了什麼損傷。

而她的必殺技計量表則整條都發出了明亮的光芒。

說來也是。都破壞了那麼巨大的地形對象，想也知道會加算非常龐大的額外加值。也就是說，紅之王之所以會胡亂發射飛彈，其實不是為了抵御春雪來自背後的攻擊，也不是企圖讓他被崩塌的建築物壓垮……

忽然間——

幾道紅光從眼下的斷垣一殘壁中迸射而出，同時聽到一聲尖銳的喊聲：

「——『飽和熱線砲（Heatblast Saturation）』!」

看到深紅色的火線發出一陣嗡嗡嗡的刺耳共鳴聲，貫穿大樓的殘骸筆直往上延伸，春雪發出了慘叫：

「哇啊啊啊啊!」

他全力振動左翼，企圖以螺旋俯衝動作閃避，然而……

火線實在太過巨大，春雪沒能完全躲過這直徑幾乎跟自己身高相等的光束，Silver Crow的左手碰到了撼動景色的高熱圈，手肘以下的部分應聲蒸發。

HP條立刻減少了整整一成五左右，同時更受到一陣如假包換的灼熱感侵襲，但春雪卻幾乎沒有意識到這些。

原因很簡單，因為他看到從自己身旁通過的熱線繼續朝場地東方延伸，將屹立在遠方的新宿都廳大廈超過三百公尺以上的部分，都轟得不留痕跡。

「不會唄……」

春雪發出了這場戰鬥中已經不知道是第幾次的驚嘆聲。

一張嘴在銀色面罩下開開閉閉之餘，春雪轉動視線，望向住家大樓的殘骸。

正好就在這時，紅之王雄偉的身影從斷垣一殘壁中開出的巨大貫穿孔洞中再次出現。

全身美麗的紅寶石裝甲看上去毫髮無傷，背面與下方推進器噴出了淡淡發光的排氣火焰，左手砲管上刻出的缺縫正冒著白煙。

「……哦哦，在飛耶在飛耶☆」

紅之王從前方裝甲的縫隙中露出渾圓的雙眼仰望Silver Crow，用唱歌般的聲調這麼說：

「這就叫做對空炮火嗎?我一直很想試一次看看，科幻片之類的電影裡就常常看到猛撒對空炮火的場面，看起來超好玩的呢。」

一聲響亮的金屬聲鏘地響起，雙肩上的飛彈發射器貨櫃全部掀開，接著更舉起右主砲，設置在前方的四門機關砲也更改了角度。

春雪全身發抖之餘，腦子裡也同樣想起了電影或動畫裡的這類場面盡小小的機器人兵器試圖鑽過敵方要塞壓倒性的對空炮火彈幕……這些人多半都會像虫子一樣被擊墜，喊著情人或是其它人的名字爆炸。

啊，那我就喊黑雪公主學姐吧。可是那只是她的綽號啊，不過就算要喊本名盡實在也有點不好意思。

就在春雪陶醉於逃避現實的思考之中時，敵人的主砲已經發出低沉的轟隆聲響開始充電，貨櫃中也露出了估計不下一百發的小型飛彈群，飛彈尋標頭的鏡頭閃出了光芒。

敵人的必殺技計量表想必已經因為破壞都廳而再次集滿，相較之下春雪則只剩5%不到，看來頂多只能全力飛行幾十秒。儘管不合春雪的興趣，但局面已經逼得他只能賭賭看神風特攻隊式的敢死衝鋒。

「……我話先說在前面，從以前開始，巨大戰艦就是會被一架機器人給打沉!」

隨著這句死不認輸的話出口，春雪在空中做出了準備俯衝的姿勢。

「變態開的機器人哪有可能這麼活躍，你白痴啊!」

紅之王先吐出一句未免說得太難聽的台詞，接著高聲大喊：

「——『雹暴肆虐（Hailstorm Domination）』!」

嗡嗡嗡啪啪啪噠噠噠，三種炮聲同時響起，主砲、飛彈跟機關砲齊射。

這種攻勢可說是集這些日子以來讓春雪陷入苦戰的「遠距離對空攻擊」之大成。不管上周還是上上周，春雪都被不到現在十分之一的火力逼得一籌莫展，慘遭擊墜。

然而不知道為什麼，現在他卻絲毫沒有放棄的想法，甚至不覺得害怕。

也許只是因為敵人實在太強大，讓他乾脆豁了出去。但現在春雪卻感受到了一種已經許久沒有感受到的感覺，一種全身血液沸騰的火熱感覺，也就是「對戰」所帶來的興奮。

「……喝啊——!」

春雪大喊一聲，先朝右方來個空中衝刺，避過了主砲射出的光束。畢竟要是被這玩意打個正著，一瞬間就會被蒸發。光束驚險地從他身旁不遠處掃過，這次則在一些立體停車場跟新宿NS大樓上開出了大洞。

然而看樣子敵人也預測到了他會做出這種機動，無數的小型飛彈尋標頭亮出有如滿天緊星的光點逼近過來。

春雪深深吸一口氣，展開了使出渾身解數的超高速機動。

先以直線飛行引來一叢飛彈，再以小於九十度的銳角變向甩開。還沒擺脫受到失去追踪目標的飛彈群爆炸衝擊，已經開始去吸引下一群飛彈，再次展開閃避行動。

Silver Crow有如飛碟似地在空中劃出鋸齒狀軌道，一路開出無數的爆炸花朵飛個不停。

不可思議的是，無論是飛彈的軌道，還是機關砲的彈幕，他都覺得自己可以看得清清楚楚，只是他也不知道這是不是在那個純白房間進行的訓練所帶來的成果。

就在極限的高速飛行之中，春雪忽然對自己產生了一股強烈的怒氣。

為什麼在每個周末的領土戰裡，我就是做不出這種動作?為什麼每次都只不過被一挺小型步槍瞄準，就嚇得兩腿——不，應該說是雙翅發軟，想飛也飛不快?如果要說是壓力太大所造成，現在這場跟威名遠播的「紅之王」展開的一對一戰鬥明明要可怕得多。

我明明這麼快，這麼能飛，為什麼每次到了重要關頭就是會呆呆中彈?我明明非得變得更強不可，非得變得更強，升到更高等級，才能跟她……

「……!」

就在春雪在面罩下咬緊牙關時，飛行速度稍微放慢，讓他沒有及時跟上唯一的閃避路徑。

還剩三十發左右的飛彈群從正面撲天蓋地似地飛來，背後則有機關砲的彈幕，在地上更可以看到Scarlet Rain的左主砲已經完成重新充電，開始捕捉目標。

「可……惡啊!」

春雪用右腳踢開了已經直逼眼前的飛彈，腳掌被爆炸炸得粉碎。接著利用爆炸的反作用力，朝著正下方展開了孤注一擲的俯衝，然而主砲的巨顎卻已經等在他眼前——

就在這時，戰場上吹起了猛烈的強風。

是「風化」場地的地形效果。水泥外露的建築物跟地面揚起了大量的沙塵，視野一瞬間被一片灰色封鎖。周圍的飛彈群跟丟了目標，接二連三引爆。

……就是現在!

春雪睜大眼睛，只看準了沙暴中亮起的紅寶石色光芒，呈螺旋狀俯衝。

已經發射的主砲貫穿俯衝軌道的巾心點，只徒然燒過虛空。

「哦哦哦哦哦哦!」

隨著一聲怒吼，春雪轉換姿勢，整個人從挺出的腳尖化為一道光線下衝，將孤注一擲的左腳螺旋踢，對準微微可以看見的Scarlet Rain兩組飛彈發射器之間的縫隙。只要這一擊能夠正中目標，就還有機會反敗為勝。

然而——

「……!」

就在有如劍尖一般鋒銳的腳尖即將觸碰到目標的時候，巨大的要塞型虛擬角色卻一口氣分解開來。

貨櫃與主砲從本體分離出去，裝甲板也往四周攤開。

嬌小的少女型虛擬角色從中出現，抬頭望向自己。

她以快得離譜的速度往旁讓開一步，躲過了Silver Crow的下墜踢。

春雪碰的一聲在地上穿出一個大洞，接著難看地摔倒在地，馬上就有個物體碰上了他的面罩。抬起頭來一看，就看到一個小小的槍口。Scarlet Rain的本體，也就是那個小不隆咚的少女型虛擬角色，右手握著一把同樣小不隆咚的火紅色手槍，槍口對準了春雪。

——剛剛那一踢被她輕而易舉閃過的時候，我就已經輸了。

儘管內心這麼想，但春雪還是死不認輸地放話：

「……妳以為那種玩具槍打得穿我的裝甲?」

結果紅之王就在她那只有兩只圓形鏡頭眼的面罩上，明顯地咧嘴一笑說道：

「如果我說這把槍才是我最強的武器，大哥哥你會相信嗎?」

春雪深深吸一口氣，呼的一聲吐了出來，接著舉起了雙手——只是左手其實已經沒了。

「……我相信，是妳贏了，Scarlet Rain。」

結果紅之王又笑了笑說：

「那你肯聽我的請求嗎?」

「咦?請求……」

該不會是要我背叛黑色軍團吧?唯有這點絕對免談。

春雪內心十分擔心，但答案卻完全出乎意料之外。少女突然粗著嗓子，傲然撂下這句話：「——讓我見你的『上輩』。直接在現實世界裡……面對面。」
