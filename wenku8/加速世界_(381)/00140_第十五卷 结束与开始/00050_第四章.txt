黑雪公主曾經數度試圖想起在「那一瞬間」，她在想些什麼，又感覺到了什麼。

兩年又十個月前——二〇四四年八月所舉辦的加速世界首屆七王會議席上，提倡七大軍團間和平協定的紅之王Red　Rider，對黑之王說了這麼一句話。

——就算我們有一天在現實世界裡相見，我跟你也能當好朋友。不，我就是想跟你當好朋友！

換個角度來想，這句台詞也可以解釋為他是想跨出這群年幼的王以往所結下的情誼，也就是想跨出超頻連線者之間的情誼，發展出更進一步的密切關係。而最先對這句話有了反應的，是當時與Rider最要好的紫之王

——等一下，Rider，你剛剛這句話我可不能聽過就算！

——不……不對，你誤會了，我不是這個意思……這可傷腦筋了。

Yhorn與Rider的互動，讓藍之王Blue　Knight與白之王White　Cosmos都莞爾一笑，連黃之王Yellow　Radio都忍不住低聲哼笑了幾聲。唯有綠之王Green　Grandee只微微讓厚重的裝甲發出幾聲輕響。

做為會議用的亂鬥空間籠罩住一片友好的氣氛中，黑雪公主忽然想到他們兩人是「上下輩」的可能性。

七王——這群過去人稱「純色玩家」的大軍團團長們，雖是從加速世界的黎明期就一直打到今天的老手，但並非每一個王都屬於「最初的一百人」。身為White　Cosmos「下輩」的黑雪公主自己當然就是例外，而儘管沒有確切證據，但白之王自己很可能也不屬於這批人。因為黑雪公主聽說的情形，是當初從神秘開發者手中得到BB城市的一百人，是在二〇三九年四月才剛升上小學一年級——也就是說這群小孩子是二〇三二年出生的，比黑雪公主和與她同學年的白之王大了一屆。

為什麼這群Originator的年齡，並不等於最高齡超頻連線者的年齡呢？對此有多種說法，其中最有說服力的一種，就是認為二〇三一年出生的兒童當中，能夠達到安裝BB程式門檻條件的只有一半左右。這是因為能夠讓嬰兒佩帶的神經連結裝置，是在二〇三一年九月上市，在這之前出生的兒童都不可能「從剛出生就佩帶神經連結裝置」。

不管理由何在，七王之中令人能夠確信是Originator的，也就只有藍之王和綠之王。

——如果紫之王和紅之王是「上下輩」，又或者是「情人」，相信接下來我要做的事，將永遠得不到她的原諒。

黑雪公主關於自身想法的記憶，就在這裡中斷。

當諸王的笑聲即將平息，黑雪公主站了起來，一邊走向Rider，一邊對和平協定表達贊同。Rider十分高興，伸出右手想握手，黑雪公主則以擁抱回應他。

Thorn更加高聲抗議，讓眾人再度大笑，而「那一瞬間」就這麼來了。

Black　Lotus的8級必殺技「死亡擁抱」射程只有短短的數十公分，威力卻是無限大。一旦交叉的雙手劍合上，存在於雙劍圈內的事物都將毫無例外被切斷，哪怕是達到9級的對戰虛擬角色裝甲也不例外。

連紅之王落在雙劍交叉點上的頭顱，以及倒在她腳下地面的身體，都化為無數細小絲帶崩解消失時，自己到底感受到了什麼，黑雪公主都不記得了。

——不……不要啊啊啊啊啊啊啊！

紫之王發出的尖叫回蕩在整個空間中。

——這就是你的選擇嗎？Lotus，

藍之王發出換了個人格似的怒吼。

到了這裡，記憶的空窗才總算結束。

十二歲的黑雪公主雙手劍犀利地往外一分，腦中想著一個念頭。

她想著，還剩四個人。

無論累積多少超頻點數，都無法從9級升上最終等級的10級。系統所要求的條件，是讓五名同屬9級的玩家喪失所有點數。

Red　Rider就是因為知道了這個規則，才會提倡諸王之間的和平協議，而Black　Lotus也才會砍下Rider的首級。在加速世界漫長的歷史當中，「9級一戰定生死規則」實際發動的情形也就只有這麼一次——應該是這樣。因為儘管黑雪公主委身於瘋狂的戰意，展開一場鬼神般的激戰，卻再也沒能拿下其他任何一個王的首級。不，也許能撐到亂鬥結束而回到現實世界，就已經該說是萬中無一的奇跡了。

之後過了兩年又十個月的現在，那一晚的記憶已經被一層淡紅色的霧靄封鎖，讓她很難想起細節，但這一切都是現實。只要打開系統選單，看看升級畫面，就會看到升上10級所需亮起的五個指示燈當中，最左端的一個已經亮起紅色的光芒。伸手去碰指示燈，更會顯示出Red　Rider這個名稱。

所以——

此時此地，初代紅之王不可能出現在這裡。

黑雪公主凝視著這個從ISS套件本體後方現身，戴著牛仔帽，配掛環形槍帶，有著西部快槍手造型的對戰虛擬角色，同時以一部分意識勉強想到這裡。

是有人扮裝成Rider，再不然就是放出了沒有實體的影像。眼前的現象屬於這兩種情形的可能性很高。

但即便理智做出這樣的推測，直覺卻痛切地感受到，無論嗓音、語氣、舉止、以及氣氛，這一切都在在顯示出這人就是初代紅之王「槍匠」Red　Rider。

快了一步從震撼中清醒過來的楓子、晶與謠，都緊緊貼到還呆立不動的黑雪公主身旁。最年少的謠當年應該極少有機會和Rider直接面對面，但與黑雪公主同樣老資格的楓子與晶，則曾在戰場上遭遇、交談、交戰過無數次。

但她們兩人無論對黑雪公主與站在二十公尺前方的紅色虛擬角色都不說話，始終保持沉默。同伴們將一切委由她判斷的意志，透過輕輕相碰的裝甲微微傳了過來。

砍下Rider首級的瘋狂夜晚，以及黑暗星雲在禁城那場兵敗如山倒的敗戰過後四個月，黑雪公主得知原來她是被自己最信任的人深深欺騙、利用。

再也不要犯下同樣的過錯。要自己去感覺、思考、選擇、決定。無論身為新生黑暗星雲的團長，還是身為現在這一瞬間在另一個地方奮戰的那位小她一歲的少年的「上輩」，她都必須如此。

黑雪公主正視靠在漆黑巨大眼球上的快槍手，說出了決定性的第一句話：

「你應該已經被我殺了，Red　Rider。」

一聽到這句話，紅色虛擬角色就流露出微微苦笑的氣氛。

「是啊，你說得對。那個時候，該怎麼說呢，真是有種從天堂掉進地獄的感覺啊。才剛想說嚴禁碰到的『絕對切斷』竟然會來抱我，結果就變成這樣。」

說著他伸出兩根手指，做出剪刀一剪的動作。無論是這段對正確的狀況描迤，還是稚氣未脫的言行舉止，怎麼看都只可能是紅之王本人。

不可能發生的事情就發生在眼前的驚愕，以及埋藏在內心深處的記憶被突然挖出來的震撼，讓她無論怎麼壓抑，身體還是忍不住發抖。但黑雪公主卯足力道灌注在雙腳，繼續站在原地。

五個月前，黑雪公主答應協助第二代紅之王Scarlet　Rain，前往討伐第五代Chrome　Disaster之際，光是看到黃之王Yellow　Radiog吨存下來放給她看的Rider永久退場場面，就引發了零化現象，

當場動彈不得。

現在眼前的現象所帶來的震撼，遠非錄影下來的畫面所能相比。然而無論發生什麼樣的情形，她都不想再那麼難看地倒下。

「……那麼就只會有兩種情形。你不是對戰虛擬角色的幽靈，就是第一個曾經損失所有點數，卻找到方法再度安裝BRAIN　BURST的超頻連線者了。」

黑雪公主說得強而有力，Rider歪了歪帽檐，做出思索的姿勢。

「說得也是，嚴格說來，我想應該是前者吧。」

「哦？這麼說來，你是放不下怨恨，才會化為厲鬼冒出來了？那正好，我們有具備淨化能力的巫女隨行，你就請她幫你除靈吧。」

身旁的謠全身一顫而縮起身體，楓子的手立刻神速閃動，阻止巫女型虛擬角色後退。她們兩人之間一如往常的默契，帶給了黑雪公主小小的勇氣。她繼續說道：

「——又或者，如果你是有話想說才出現，那我就聽。因為你……應該是有這個權利指責我的。」

黑雪公主當然也並非認為現身的是真的幽靈。因為即使從加速世界消失，以前曾是紅之王的少年現在應該仍在現實世界的東京生活，而且還完全喪失了自己曾是超頻連線者的記憶。

但相對的，在加速世界裡，不管發生什麼都沒有什麼不可思議的——儘管這句話終究只在系統容許的範圍內成立。就連黑雪公主這種最老資格的玩家，都尚未看清BRAIN　BURST馗款全感覺型對戰格闘遊戲的全貌。所以數位幽靈以遊戲系統容許的形式現身的這種情形，或許是有可能發生的。

聽黑雪公主這麼問，Rider仍然靠在套件本體上，雙手環抱到胸前回答：

「這個嘛，說我是有話想說才現身，這句話是沒說錯，但我可不是為了你想像的那種怨恨才現身。畢竟現在的我，已經知道當初你不惜用偷龔砍下我首級的真正理由了。」

「……你說什麼？」

黑雪公主被這短短幾分鐘內已經不知道是幾次的莫大震驚震懾住，茫然說出這句話。

黑雪公主打倒紅之王的真正理由。

相信幾乎所有的超頻連線者，都認為她是想比任何人都搶先升上10級。這的確是事實，卻不是事實的全貌。在那樁慘劇背後，有個人物告訴了黑雪公主，說紅之王以「創造槍械」創造出手槍型強化外裝「Seven　Roads」，而這些槍更是足以讓加速世界永遠停滯的終極破壞兵器。

但這個真相，黑雪公主只對黑暗星雲的「四大元素」，以及她的「下輩」Silver　Crow說過。Rider不可能從他們口中知道這個消息，而且等她說出這件事時，Ride、早已喪失了所有點數。

……不，嚴格說來，還有一個人知道這一切的事實。這個人就是巧妙騙過黑雪公主，完美地操縱了她的那個「人偶師」。

當黑雪公主想到這裡，之前一直保持沉默的晶小聲說了句：

「顏色。」

「Current，你怎麼了？」

楓子迅速問道，接著晶以更小的聲音說：

「這個虛擬角色……顏色開始變了。」

一聽到這句話，黑雪公主就仔細凝視站在二十公尺前方的快槍手。這個靠在ISS套件本體站著的對戰虛擬角色，裝甲色就和記憶中的Red　Rider完全一樣，是純粹得獨一無二，拒絕任何形容詞的紅……

不對，仔細一看就發現晶說得沒錯，裝甲顏色從落在暗處的雙腳開始慢慢改變。從渾濁血液般的暗紅色，經過晚霞的紫色，最後變成像是焦炭的消光黑。

Rider似乎感受到了她們四人的視線，跟著低頭望向自己的雙腳，輕輕啐了一聲。

「嘖，已經來啦？我本來還以為可以再撐個三分鐘……」

「……你這話是什麼意思！你真的是Rider嗎？你出現在這裡，到底是想說什麼……！」

完全掌握不到狀況的焦慮，驅使黑雪公主大聲吶喊。

但已經連腰部附近都變黑的快槍手並不回答她的這些問題，只輕輕拉起帽檐，像是在表達歉意。

「不好意思啊，Lotus，我們先打一場再聊。」

「什……什麼……？」

「你聽好了，一定要打贏。要比那個時候贏得更不留情、更徹底。因為讓這玩意用掉愈多能量，我就能維持自我愈久。」

「……你說要打贏，到底是要打贏誰？」

「當然……是要打贏我啊。」

紅之王攤開雙手，輕輕聳肩。這時他已經從胸口到頸子，隨即連面罩都染成焦炭色。連有著淺淺V字形刻痕的護目鏡內，都充滿了黏稠的黑暗。

護目鏡下發出一聲令人不舒服的震動聲，然後亮起了血色的光芒。這一瞬間，這個個子也不怎麼高大的虛擬角色全身散發出足以彌漫整個樓層的殺氣，吞沒了黑雪公主等人。

這種無底的飢餓，卻又帶著點無機質的波動，黑雪公主並不陌生。就在短短幾十分鐘前，他們才在中城大樓北方的平地上和Megenta　Scissor那群使用ISS套件的部下打過。當時那群人身上籠罩的就是同一種性質的鬥氣。

擠在一起的四個人一時間全身僵硬，而這個空檔似乎就被對方盯上……

Red　Rider連指尖都完全黑化後，雙手以快得令人看不清楚的速度閃動，抓住掛在槍帶兩側的雙槍，拔了出來，舉槍瞄準，這一連串動作所花的時間幾乎等于零。

***

——趕上了。

——仁子還存在於這個世界。

美早一看到被定在黑色十字架上的Scarlet　Rain，心中涌起的是一股輕柔液體般的安心感。但就在零點一秒後，一陣壓倒性的怒氣點燃了這些液體。Rain身上裝甲隨處可見淒慘的裂痕，而且似乎還失去意識，鏡頭眼黯淡無光。何況她還像個被處決的罪人一樣，整個人攤開雙手被固定在十字架上。美早不能容許任何人對紅之王——日珥的首領如此無禮，萬萬不能。

邊長將近五十公尺的中庭正中央站著Vice與Argon，東側則站著Silver　Crow、Lime　Bell與CyanPile。這三名黑暗星雲的新秀面對實力深不可測的加速研究社兩名最高幹部，仍然堂堂正正地與他們對峙。這場戰鬥不是只屬於美早一個人，開啟戰端的權限，應該交給一路追著Vice來到這裡的Silver　Crow。

美早再度朝仁子送出視線，在內心發誓一定馬上救她下來，然後往幾乎正下方的方向跳了下去。接著斜向切過中庭，移動到Crow身邊，只輕聲說了一句：「久等了。」

其實抵達這裡花的時間比她預計的還要久。雖然藉助了Cyan　Pile與Lime　Bell的力量衝入影子隧道，但卻在完全的黑暗中與兩人失散，被衝到了別的出口。

但所幸——不知道該不該說是幸運，她發現了Argon跑在昏暗通道前方的身影，於是悄悄跟了上去，就這麼穿過了迷宮般的地下樓層。來到地上後雖然被對方發現自己在追踪，但Argon似乎以和Vice會合為優先，一發雷射也不射就一路跑到這裡，所以美早現在才能在決戰之地和同伴會合。

加速研究社的大本營竟然如此寬敞明亮，而且在現實世界中似乎是一問大規模的學校，說來的確驚人。然而這種情報分析的工作大可留到日後再做，現在她應該把全副心力都集中在一件事上，那就是擊破眼前的兩名強敵，救回仁子。留在中城大樓的黑之王等四人，應該也已經抵達最靠近的傳送門，但要從傳送門回到現實世界，以血肉之軀拔掉仁子的傳輸線，不管動作多快，相信至少都得花上三秒鐘左右，相當於這個世界的五十分鐘——如果能在這之前擊破Vice等人固然好，即使辦不到，至少也非得保住仁子不可。

美早重新下定決心，將雙手鈎爪伸到最長，緊接著……

在轟隆巨響中從天而降的第七個超頻連線者，讓戰局變得更加混亂。

美早起初不知道這個中途插手的人是誰，但Silver　Crow以沙啞嗓音喊出的名稱，在她聽來並不陌生。

Wolfram　Cerberus，突然出現在加速世界中的超級新人。儘管上輩與所屬軍團都不詳，卻靠著一點都不像初學者的戰鬥才能與令人驚奇的防御性特殊能力「物理無效」，甚至有多名中等級玩家陸續遭他擊破。即使身在練馬區，也聽得到這位對戰天才的傳聞。

由於Cerberus主要是在藍之團或綠之團的領土內出現，美早尚未直接看過他打鬥的身手，但一直覺得應該想辦法看個一場。她萬萬沒想到會在這種狀況下，才第一次遭遇到他。

最大的問題在於Cerberus是敵是友。如果他是敵人，從某個角度來看，也許比Vice或Argon更需要小心提防。畢竟我方的三名攻擊手都屬於只有物理攻擊的近戰型。

美早瞬間想到這裡，而她的擔憂在一秒鐘後成了現實。

灰色的金屬色虛擬角色起身後，背對Vice與Argon，與Silver　Crow正面對峙。

Wolfram　Cerberus是敵人。也就是說，他是加速研究社的成員。

美早將這個認知牢記在心，但Cerberus的第一句話，卻有些出乎她意料之外。

「……我真不想用這種方式跟你再會……Crow兄。」

他沉痛的聲調怎麼聽都不像是演出來的。而Silver　Crow回答的聲調，也令美早覺得蘊含了與震驚等量的痛苦。

「我也一樣啊，Cerberus。你會出現在無限制空間，也就表示……你升級了？」

Cerberus點頭回答Crow的問題：

「是。不是最低限度的4級，而是跟Crow兄一樣的5級。」

「這下我們連等級也對等了啊。可是……既然如此，你維持—級賺點數的『任務』不就結束了嗎？我想跟你正常對戰，想跟你打那種不用牽扯任何恩怨的純粹對戰。所以……Cerberus，請你不要站在這裡。」

Crow的聲調雖然經過壓抑，但仍然帶有迫切的訴求。聽他這麼說，這名小個子的虛擬角色微微搖了搖頭：

「對不起……我不能離開這裡。可是，Crow兄在星期四那場亂鬥中對我說的話，真的讓我很高興。還有你願意在現實中見我，也一樣讓我高興。」

「……你不必把這些變成過去式。以後我們要見面幾次都行的……只要你希望。」

灰色與銀色兩名金屬色虛擬角色的對話，聽起來就像用指甲去彈綳緊到極限的極細鋼絲。由於鋼絲往兩個方向拉撐，隨時都有可能綳斷。他們的對話就是這麼純真而岌岌可危。

「剛才Crow兄說得沒錯……他們賦予我的任務幾乎已經完成了。這也就意味著，他們允許我存在的理由也跟著消失了。相信今天過後，我就再也不會有機會像這樣和Crow兄說話……」

Cerberus說到這裡，美早感覺到他狼頭狀的面罩下滲出了淡淡的微笑。相對的Crow緊緊握住雙拳，以更劇烈的聲調呼喊：

「Cerberus，你不需要什麼，賦予你的任務』或是『允許你存在的理由』這種東西！既然身為超頻連線者，目標這種東西就應該自己找出來，不是嗎！」

「………………」

Cerberus垂下頭去，並不立刻回答Crow的問題。

這時接過話頭的，是Argon　Array。她的聲調一如往常，帶有一種令人不悅的笑聲。

「啊哈哈，Crow同學，你說的這幾句話可真帥氣！可是啊，你終究是個大少爺。像什麼『既然身為超頻連線者』這種話，我可說不出來！」

Argon取笑完，他身旁的Vice就出言制止：

「Array，不可以說這種話。你自己不也有一兩個目標嗎？」

「那當然，我不會說我連三四個目標都沒有，可是就是因為其中一個就會在今天達成，我才會不惜搞得自己這麼慘痛還奉陪到現在。哎呀呀，這準備工作真的很漫長……真的是夠辛苦的了……」

「要說這種話還太早了點。站在我的立場，是希望你能在有更多人來礙事之前就動手。」

兩名幹部之間的對話充滿了謎團，但美早特意把這些對話從意識中隔絕出去。Argon說的話本身就是一種誤導型的間接攻擊，她自己就才剛付出慘痛的代價而學到這一點。一定要在被誤導之前就搶先攻擊才行。

美早對身旁握緊雙拳站著不動的Silver　Crow短促地輕聲說：

「……Wolfram　CerIoerus可以交給你嗎？」

儘管花了點時間，但Crow的回答很明確。

「好的，他就由我來對付。」

「K。我去解決Argon。Pile和Bell……」

美早視線一瞥，藍色的大型虛擬角色與綠色的小型虛擬角色不約而同點點頭，分別回答：

「我們負責Vice是吧？他是個強敵，但現在應該少了一隻手能用。我們會想辦法頂住，讓Crow和Pard小姐有時間對付強敵。」

「Pile，支援就包在我身上。我會一直幫你補血的！」

聽到兩名5級超頻連線者回答得這麼令人放心，美早以小小卻充滿堅定心意的動作點頭回應。

如果只看等級合計值，敵方是8+8+5等於21，我方則是8+5+5+5等於23。若是在「對戰聖地」秋葉原BG——雖然那裡進行的團體戰不能有任何一方超過三人——相信押美早活一方得勝的人應該會比較多。但Wolfram　Cerberus蘊含了無法以數字衡量的實力，而且其他兩人也是一樣。美早才剛升上8級，若是覺得自己和早已達到這個等級的Argon與Vice有同等的實力，那就未免太自大了。

但相對的，黑暗星雲的三人也是一群真正的闘士，能在關鍵狀況下發揮超出等級的實力。Lime　Bell擁有「回溯時間」這種令人驚奇的能力，Cyan　Pile厚重的裝甲下有著出類拔萃的智慧，而Silver　Crow的潛力更是深不可測。只要是和他們並肩作戰，相信；疋也能克服這道難關。

然後救回仁子——救回自己誓言永遠效忠的軍團長。一定。

美早讓身體微微前傾，發動了長年來一直封印的心念系統。強烈得足以讓夕陽的紅色都顯得褪色的鮮血色過剩光，從有著尖銳鈎爪的只手迸射而出。

中庭的上空似乎也在呼應他們七人劇烈升高的鬥氣，氣候微微轉音，發出低沉的雷鳴。

***

「槍匠」。

初代紅之王這個綽號，來自於他那獨一無二的能力——能夠無限創造出槍型的強化外裝。

但若要說Red　Rider就因此屬於其他網路遊戲中的「生產職業」，那就完全不是這麼回事。他之所以能夠贏得Master這個美名，有一半是來自他卓越的槍法。

全身都已染成黑暗色的Rider，以快如電閃的動作拔出雙槍，毫不猶豫地扣下扳機。中城大樓四十五樓寬廣的樓層空間中接連發出巨響，但這時黑雪公主等人也分別往左右大步跳開。

紅之王的主武裝，是一對有著專用名稱「太陽神與黎明女神」的轉輪手槍。彈筒的裝彈數是六發，所以兩把手槍合計可以連開十二槍。紅之王左右手交互扣下扳機，在短短兩秒鐘內就打光了所有子彈。射出的槍彈中，有八發從往左跳開的黑雪公主與晶，以及往右跳開的楓子與謠之間穿過，但剩下四發則以驚人的精準度，逼向四人的要害——心臟。

「嗚……！」

黑雪公主咬緊牙關，左手劍一揮，打下了瞄準她與晶的子彈。右側則可以看到楓子以左右掌擊粉碎兩發子彈。儘管避免被子彈在軀幹上打個正著，但要毫髮無傷擋下槍彈是不可能的。每次防御都會受到硬削式的損傷，讓體力計量表微量減少，相信楓子也是一樣。

Red　Rider打光了彈筒中的子彈後，以順暢的動作將雙槍朝上一舉，在喀啦兩聲輕巧的聲響中甩出彈筒，讓空的彈殻紛紛落下。

「趁現在！」

黑雪公主對同伴們大喊，猛然往地上一蹬。

直到現在，仍有一部分意識就像沒分成資料夾整理的磁碟一樣混亂。突然出現的Red　Rider到底是真是假？他那幾句神秘的話又意味著什麼？他為什麼會突然變色展開攻擊？心中的疑問只增不減。

但黑雪公主內心深處也早已有了覺悟，知道一旦逼近ISS套件本體，必然將要面對自己的過去。因為早在從Silver　Crow托付給她的封印卡上發現雙槍交叉的徽章時，就顯然表示這件事與初代紅之王有某種關連。

儘管她終究沒料到會以這麼直接的形式和Rider照面，但事態不容許她因而逃避。既然Rider說要打，那麼打就是了。她該做的事，就是把那一天因為黑雪公主偷襲而在短短一秒之內就結束的「對戰」打下去。

「喔……喔喔！」

黑雪公主毅然決然地大喊，舉起右手劍。

紅色系虛擬角色最大的破綻，就是子彈打完的瞬間。這個理論即使是紅之王也不例外。

彈筒排出空彈殻後，本來應該必須用手一發一發重新裝上子彈。然而幾乎就在排出彈殻的同時，Rider雙臂上的裝甲打開，從中出現附有機器手臂的高速裝彈器。兩條機械手臂往上伸長，各將六發子彈打進空的彈筒之中，接著就是喀啦一聲輕響，彈筒甩回槍身，完成了重新裝彈的動作。從Rider舉槍朝上，到裝彈器完成任務而收進手臂，只花了短短兩秒鐘。這是紅之王的特殊能力「自動裝彈」的效果。

以前Red　Rider還在第一線浴血奮戰時，就以這快速的槍法與裝填速度，絲毫不讓藍色系虛擬角色近身，連對上穩穩擺好防御態勢的綠色系虛擬角色，都能只靠硬削的損傷加以擊倒。即使黑雪公主對衝刺速度有自信，仍然有幾次就差那麼一點，沒能將他納入劍的攻擊範圍而敗在連射之下。

但這次所幸開始戰鬥時的距離較短，只有二十公尺左右，而且等紅之王再度舉槍瞄準時，黑雪公主已經揮劍下劈。眼看這能將碰到的一切事物都一刀兩斷的「終結劍」，就要砍上Rider不設防的頸子——

然而……

在黑雪公主意料不到的地方，有那麼極短暫的一瞬間，身體深處微微顫抖了一下。震動讓這一劍的軌道偏離，刀刃並非砍上脖子，而是砍在Rider那以紅色系來說算是有著堅固裝甲防護的左肩上。鏗的一聲金屬聲響響起，傷害特效光四濺。或許是因為肩膀被深深砍了一劍，讓Rider左手手槍停下動作，但他仍以讓人絲毫感覺不出受傷的動作，將右手手槍筆直對向黑雪公主的胸口，手指以機械般精密而冷酷的動作扣下了扳機。

槍口開出火焰花朵，幾乎從零距離發射出來的槍彈飛向Black　Lotus胸部裝甲——但她在千鈞一髮之際回過左手劍護住胸前，擋住了這發子彈。

又是一聲刺耳的金屬聲。劍脊部分激蕩出色彩鮮明的火花，體力計量表再度微幅減少。而Rider當然不會只開一槍就結束，他以機槍般的速度連續扣下扳機，第二發、第三發也精確地命中同一個位置。儘管處於系統判定為防御成功的狀態，但每次左手傳來衝擊，計量表減少的幅度就會不斷擴大。這是因為槍彈連續打在同一個點上，讓劍刃所受的損傷不斷累積。但即使知道是這麼回事，卻又不能移開護住要害的劍刃。

黑雪公主被槍彈的衝擊往後推開，同時痛切自覺到導致剛才那一劍軌道偏離的震動，是來自什麼原因。

那是她壓抑在內心深處將近三年的恐懼、是後悔，更是一種罪惡感。她對以偷襲方式讓紅之王喪失所有點數的自己所產生的嫌惡，讓推動虛擬角色的鬥志有了那麼一瞬間的退縮。原因就和半年前的零化現象一摸一樣。

——太難看了！

——我答應過他……答應過春雪！說我什麼都不會再怕，再也不會逃避過去……說無論中城大樓裡有著什麼樣的事物等著我，我都不會退縮半步！

就在黑雪公主痛切暍斥自己的同時，Rider右手手槍「太陽神」打光了子彈。被六發子彈單點連擊的左手劍，發出啪啦一聲微小但危險的聲響。

然而劍刃仍然存在，並未折斷。她的心也是一樣，雖然顫抖，但並未屈服。她要挺身而戰，對抗眼前的敵人——更要對抗自己的恐懼。

黑雪公主雙腳尖端喀的一聲插進地板，停止後退。

紅之王自動裝填右手槍之餘，再度舉起被斬擊逼退的左手槍。

太快了。絲毫沒有破綻。他明明應該是紅色系的終極型，對付起來卻像是個雙手都拿著近戰武器的藍色系對手。

她應該也可以選擇放棄近戰而大幅度後退，和楓子與晶組成防御陣形，把攻擊的工作交給謠的長弓。然而Rider變黑之際說了一句話，說要黑雪公主贏得徹底。還說這樣一來「我就能維持自我愈久」。她怎麼想都不覺得靠以智取勝的戰法能夠達成這個條件，而且更重要的是就心情上而言，現在後退就等於輸了。

黑雪公主自認並不是因為固執而拒絕同伴協助，但至少也得砍中一劍。至少得砍中能令自己滿意的一劍，否則就不能退後。然而要再度將Rider捕捉到劍刃所及的范國，只靠防御槍彈是不行的。她必須預判出槍彈發射的時機，一邊閃避一邊前進。

…………學姊。

她忽然覺得耳邊有個小小的聲音對她說話。

…………不可以看槍口。要看拿槍的對手，從他的全身預判出開槍的跡象。

————我會試試看！

黑雪公主下意識地在腦海中這麼一回應，就將視線從握在Rider手中的「黎明女神」那黝黑的槍口移開。

紅之王全身都被侵蝕成黑暗的顏色，先前談話中感受到的些許生機已經完全消失，然而這也讓那飢渴的殺意變得極為明顯。就在那V字形護目鏡下，兩個血色光點微微閃爍的瞬間……

黑雪公主一邊往前縱身而起，一邊卯足全力往地面一蹬。

槍口噴出火苗。射出的子彈掠過她頭盔上往兩側延伸的天線狀零件。Rider一邊連射，一邊把準星往下移；黑雪公主則把身體壓得更低，試圖鑽過火網。在加速過的感覺裡，每一聲槍響響起，就有以接近音速的速度飛來的金屬塊體掠過背上的裝甲。

就在身體壓低到再低一寸都會跌倒時，第六發子彈擊碎裝甲護裙的邊緣而往後方穿出。

儘管無法捕捉在視野之中，但相信Rider右手手槍已經重新裝填完畢。攻擊的機會只有一次。下次非得甩開深深刻進內心深處的恐懼，送上嘔心瀝血的一擊不可。

「喔喔喔喔喔！」

黑雪公主勇猛地吼叫，攤開雙手劍。

從壓低到極限的姿勢，不可能揮出像樣的斬擊。她沒有時間舉起雙手劍，要換成腳踢又太花時間。黑雪公主只有一種招式能從這麼接近的狀態使出。

她再度蹬地，讓身體往上彈起。以頭錘頂回Rider試圖往下瞄的「太陽神」槍身，讓雙方的虛擬身體完全貼在一起。她雙手繞到Rider身上，讓雙劍劍尖交錯。

儘管脖子和軀幹有別，但三年前黑雪公主為了創造出與現在同樣的狀況，還特地用言語和態度欺騙了紅之王。

但這次不一樣。從第一波連射算起，她足足閃躲、彈開、鑽過多達二十四發的槍彈，才讓兩者之間的距離變成零。

——我已經，不是以前的我了！

黑雪公主將所有的意志力灌注在雙手上，揮開恐懼大喊：

「『死亡擁抱』!」

深紅色的閃光呈一字形水平閃過。一聲毅然決然，毫無妥協餘地的特效聲響高聲響起。

Black　Lotus的8級必殺技，正好在紅之王槍帶的高度上，將他的軀幹斷成上下兩截。Rider應該已經受到加速世界有可能發生的最大限度劇痛侵襲，卻一聲不吭，還想繼續發射右手的手槍。對幾乎所有對戰虛擬角色來說，完全切斷頸部都是足以當場斃命的損傷，但受到腰斬則有可能存活下來。相較之下，黑雪公主則受到系統面發動完大招的僵硬狀態，無法立刻行動。

但就在槍口即將噴出火苗之際，從後方飛來的水滴精準地打中了Rider的護目鏡。水滴化為霧氣，封鎖住視野，阻礙了槍彈的發射。是Aqua　Current抓起終於慢慢恢復的水流裝甲當中的一部分，當成投擲武器攻擊。

緊接著一枝火紅燃燒的火焰箭插上Rider右手。是Ardor　Maiden以過人的弓術射穿虛擬身體的「核心」，讓握住「太陽神」的手暫時麻痺。即使被逼到這種狀況，紅之王仍然不吭一聲，

試圖幫左手的「黎明女神」重新裝彈。

這時化為一陣疾風飛來的，是讓一頭青銀色頭髮隨風飄逸的Sky　Raker。她多半是讓疾風推進器瞬間噴發，以正常跳躍不可能達到的速度直逼紅之王。

「喝！」

接著在短短一聲呼喝中，以右手掌擊高聲打到紅之王身上。Rider承受不住猛烈的衝擊，胸部厚重的裝甲呈放射狀碎裂而灑出炭屑似的碎片，但紅之王仍以駭人的鬥爭本能，試圖將雙槍舉向身前。

這時黑雪公主的僵直狀態總算結束。她利用先前壓低的姿勢，順勢高高躍起。朝著Rider那像是在空中流過的上半身垂直踢起右腳劍。

「喝啊！」

在空中劃出的這道藍色眉月軌跡，從Rider的腹部掃到頭部，將他的註冊商標——牛仔帽一刀兩斷。

而這一踢成了最後一擊。Red　Rider的體力計量表——前提是真的有這種東西存在——耗盡，試圖後仰的上半身在空中不自然地停住，緊接著化為無數黑色碎片爆裂四散。

這些碎片飛散途中逐一化為漆黑的煙霧而消失之際，黑雪公主一個直體後空翻輕巧落地，慢慢轉身面向從後方跑來的三名同伴。

「……小幸……」

聽楓子輕聲呼喊，黑雪公主把胸中累積的空氣，混在一句短短的話裡吐了出來：

「——是冒牌貨。」

「…………」

她回視盯著她的三名同伴，微微放緩語氣說下去：

「……Rider的強悍不是這傢伙能比的。哪怕是以一敵四，他也不會退後一步，只會開槍開槍再開槍，把敵人一個個擊倒、擊潰……Red　Rider就是這樣的人。即使射擊技術重現出來，每一發子彈當中灌注的氣勢卻完全不一樣。他不可能是真的Rider。」

「可是，裝甲色改變以前的氣氛，還有說話口氣，都跟『槍匠』一模一樣。」

晶指出這一點，黑雪公主先點點頭，隨即又搖了搖頭。

「嗯……可是……我也說不出哪裡不對，就是不對勁。『對9級勝場數』也沒增加。」

黑雪公主一邊瞥向視野左側的系統訊息區，一邊說出這句話——

而回答她的不是楓子，不是晶，也不是謠。

「哈哈哈，要是會增加，那可不得了。豈不是弄得只要在這裡陪我玩上幾場，就可以升上10級了？」

「————！」

四人迅速轉身，看見的是盤踞在樓層南側的巨大眼球——ISS套件本體，從瞳孔部分吐出一個虛擬角色的光景。不用去看那牛仔帽與大型槍帶，也知道這人就是幾十秒之前才剛被她們打倒的Red　Rider。

「這……這是，怎麼回事……？」

楓子凝視著這名正將身體從半透明黏膜中抽出的虛擬角色，發出了驚呼聲。Rider下到地上，踩響裝了馬刺的靴子開始走來，以悠哉的聲調開了口：

「我不是說過我就和幽靈差不多嗎？從某個角度來看，我是真的Red　Rider，但同時卻又是假得不能再假的冒牌貨……要讓你們理解這一點，就得先打過一場。抱歉剛剛那樣猛開槍打你啊，Lotus。」

「哪……哪裡……彼此彼此，可是……」

黑雪公主先茫然說到這裡，這才振作起來，尖銳地喝道：

「……管你是真貨假貨，別再老是說些神秘兮兮的話了！你到底是什麼東西？是怎麼出現在這裡，又怎麼再生的？」

「別這麼生氣，我全都會告訴你們。而且多虧你們徹底毀了上一個傢伙，這次我應該可以維持『自我』好一陣子……」

紅之王說到這裡，似乎想表示自己無意開打，在離黑雪公主等人約有十公尺的地方停下腳步，在大理石地板上盤膝坐下。他用手指打了個手勢要她們過去，於是黑雪公主等人小心翼翼地接近。

黑雪公主來到輕聲細語說話都能聽見的距離後停下腳步，Rider抬頭看著她，以鎮定的聲調說出了駭人的話：

「我是幽靈沒錯，可是我不是自願變成這樣的。是有個超頻連線者，讓被你砍下頭顱而損失所有點數的我『在受限的情形下起死回生，做為實現這人目的的零件』。」

「起……起死回生……？不是體力計量表變成零而死掉的情形……是讓喪失所有點數，從加速世界徹底消滅的超頻連線者……起死回生……？」

黑雪公主反問的聲調嚴重沙啞而戰慄，幾乎讓她無法相信那是自己說話的聲音。站在她左右兩側的楓子等人也都說不出話來，只能呆站在原地。

不可能。超頻連線者一旦喪失所有點數，就會引發「最終消滅現象」，永遠離開加速世界，安裝在神經連結裝置內的BRAIN　BURST程式也將完全被刪除。

說得精確一點，這種現象並不是發生在點數變成零的瞬間，而是發生在點數為零的狀態下接受戰鬥勝敗判定的時候——舉例來說，即使在商店不小心買到讓點數剛好歸零，只要能在下次對戰結束結束前恢復就可以免於強制退出——但紅之王的情形是因為適用9級玩家間一戰訂生死的規則，當場損失所有點數，所以理應不可能有救。要讓他「復活」當然也是不可能的。

Rider將黑暗星雲的四名首腦人物推進驚愕的漩渦，卻只在地板上輕輕聳肩，追加說明：

「我不是說過是『受限的情形下』嗎？現在的我，就像是三年前和你打過而消失的RedRider的『影子』……說穿了就是僵屍。現實世界中的真貨，多半已經忘了加速世界的所有記憶，正過著和平的學生生活吧……雖然我對那個關鍵場面沒有記憶，所以也不能斷定。但我想多半就是有人用必殺技，又或者是心念，創造出了一個我靈魂的複製體之類的玩意兒，然後讓這複製體附身在那玩意兒……附身在後面那顆大眼球身上。」

「讓靈魂的，複製體……去附身……」

黑雪公主復誦這幾個字眼，謠在她左側微微搖頭，輕聲說：

「如果這是真的……就不能說是起死回生。複製離開加速世界者的靈魂，為了自己的目的而加以利用……這種能力不是『復活術』，應該稱作『死靈術』。」

「的確……可是——為什麼？這個人為什麼要做這種事……？」

紅之王的「影子」靜靜地回答了楓子的疑問：

「當然是為了利用Red　Rider的能力。這人就是為了利用特殊能力『創造槍械』來製造想要的強化外裝，才會創造出我這個Rider的僵屍，然後讓我寄生在那顆拿來當肉體的眼球上。」

紅之王所說的話實在太令人震驚，黑雪公主光是咀嚼他提供的情報就已經費盡心力，但仍然立刻聽懂了剛才他那番話中所提到的「強化外裝」指的是什麼。

「……就是ISS套件，沒錯吧？那種被人拿到加速世界中散播的眼球，就是靠你的特殊能力創造出來的……就是這麼回事吧……？」

「我話先說在前面，無論設計還是規格，可都不是我決定的。而且當我被大眼球吞沒的時候，該怎麼說呢，就像主開關關掉，不能看、不能聽，也不能思考。只有大眼球大量消耗能量，進入回復模式的時候，我才能像現在這樣現身。現在就是屬於這樣的狀況。」

「……也就是說，眼球為了讓剛才被我們打倒的你重生，消耗了很多能量？所以你才要我們贏得徹底？」

「就是這樣。順便告訴你們，第一個我之所以能出來，是因為大眼球為了防衛，創造出了一大堆小眼球。只是這些小眼球也都被你們解決了就是。」

「原來如此……也就是說，等那個大眼球的能量恢復夠了，你又會像剛才那樣變黑，然後攻擊我們了。」

她問這話只是為了問清楚實務上的問題，但Rider賠罪似的輕輕舉起牛仔帽。

「不好意思，就是這麼回事。只是比起真正的我，應該是弱得多了。可是目前我似乎還能再聊一陣子。」

如果Rider的說明是事實，她們就必須趁現在盡可能取得情報。但想問的事情實在太多，讓黑雪公主一時間排不出優先順位。她困在制那間的迷惘之中，而晶打破了這短暫的寂靜：

「追根究柢來說，那玩意兒到底是什麼？」

她說到「那玩意兒」三字時，望向後方的巨大眼球型物件問出這句話，而紅之王給出了明快的回答：

「看樣子不是公敵，也不是強化外裝，多半是對戰虛擬角色。」

這個回答帶來了更大的震驚，讓黑雪公主、楓子與謠同時倒抽一口氣。但晶則不愧是BloodLeopard的「上輩」，毫不浪費時間，繼續問了下去：

「也就是說，控制那顆眼球……控制ISS套件本體的人，跟我們一樣是超頻連線者？」

「問題就在這裡啊。如果是公敵，又沒受到馴服，不可能就那樣乖乖不動；如果是強化外裝，在『變遷』的時候應該就會消失。而且那個大眼球裡，的確有著像是感情或意志之類的東西。只是……如果那是超頻連線者，我就搞不懂這人怎麼可以這麼長的期間，都一直連進無限制空間不出去。我現在是僵屍，平常感覺不到時間流動，可是讓我附身的那玩意應該就不能這樣了。畢竟從那玩意出現在這裡以來，在這個世界裡至少已經過了五十年。」

「五……五十年？」

這個答案令她忍不住驚呼出聲，但從ISS套件的存在在加速世界中曝光以來，已經過了現實時間的兩周以上。照算下來，在加速世界裡的確已經過了將近五十年的時間。無論考慮遊戲內或遊戲外的情形，要連續連線這麼久，幾乎是不可能的。

而且像這樣直徑長達三公尺的球體，表層有著狀似腦髓的肉質裝甲，沒有手也沒有腳，只有一個巨大的眼睛，而且還把傳送門吞沒到身體內側，這樣的對戰虛擬角色真的有可能存在嗎？到底要從什麼樣的「精神創傷」，才會塑造出這樣的虛擬角色……？

心中既覺得不可能，同時卻又覺得說不定真有這個可能。散播ISS套件的是加速研究社，他們曾經實際塑造出「災禍之鎧」Chrome　Disaster，破壞「赫密斯之索縱貫賽」，甚至馴服神獸級公敵「大天使梅丹佐」。如果是由這個組織策劃，做出什麼事來都沒什麼稀奇。相信將喪失所有點數的Red　Rider化為僵屍加似利用的人一定也是研究社的成員。

沒錯，最重要的是加速研究社曾以「掠奪者」Dusk　Taker為尖兵，趁黑雪公主不在而大舉肆虐梅鄉國中，讓有田春雪、黛拓武與倉嶋千百合等三人深受其害。加速研究社是為了在加速世界中製造大規模的混亂與紛爭而活動。這樣的一群人，也許真有辦法以目前旁人甚至無法想像的手段，創造出像ISS套件本體這樣的對戰虛擬角色。

「…………Rider。」

黑雪公主以拋下震驚與迷惘的聲音，呼喊著這位以前的朋友，同時也是自己親手斷送掉他性命的超頻連線者。

「我……我們非得完全破壞那顆大眼球不可。不管這人是不是對戰虛擬角色，也不管……結果會讓現在存在於這裡的你消失，我們都非做不可。」

這位還坐在地上的快槍手聽完，流露出些許的苦笑。

「我怎麼可能叫你們別動手？現在的我只是個在模糊的意識中，被迫一心一意製造這些狗屎強化外裝的僵屍。我一直在等，等待有人能幫我做個了結。而這個人是Lotus你，正是……」

紅之王不說完這句話，又聳了聳肩膀，換了種口氣說：

「不過啊，一旦真的開打，那玩意兒可是很強悍的。雖然連我也不知道那玩意兒會使出什麼樣的攻擊，但唯一可以確定的就是強得不像話。別以為那只是擺設，從一開始就要火力全開。」

「……我明白了。」

黑雪公主一點頭，Rider就拉了拉帽檐，先伸直雙腳，才以輕快的動作站起。Rider正要轉過身去時，晶叫住了他。

「『BBK』，再告訴我一件事。」

「這綽號真讓人懷念。『純水無色』，你要問什麼？」

「用你的能力製造出來，被散播在加速世界中的那些ISS套件，在破壞本體之後都會死掉嗎？」

這個問題的確應該問清楚。現在她們的最優先事項，是從被套件本體吞沒的傳送門脫離現實世界，幫被Black　Vice綁走的仁子拔掉傳輸線。但今天之所以會進行這場任務，最原本的目的就是要讓ISS套件終端機失去作用，阻止套件繼續干涉感染者——尤其是不能讓套件繼續干涉留在梅鄉國中保縣市的Ash　Roller，也就是日下部綸子。

可是————

Rider聽晶問起，一瞬間停下動作，接著用力搖了搖頭。

「不……我想就算大眼球死了，小眼球也不會跟著陪葬。這大眼球跟我製造出來的小眼球，的確透過那個傳送門相連。而大眼球一旦被消滅，那種像是交換資訊的連線應該也會斷絕，可是小眼球本身應該會留下來。」

「這……這怎麼可以！那Ash會……！」

謠以細小的聲音驚呼。相信黑雪公主也同樣受到了衝擊，畢竟她們之前一直深信不疑，認為只要破壞本體，終端機也會跟著死滅。

但這時晶發出了雖顯僵硬卻仍然冷靜的聲音：

「只要這所謂交換資訊，也就是精神干涉可以先停下來，應該就可以暫緩當下的危機。至於怎麼消滅套件，以後大家再慢慢討論就好了。」

「啊……的……的確是這樣。如果有時間可以慢慢處理，我一定會把寄生在Ash身上的套件淨化……」

謠充滿鬥志地說到這裡，紅之王就打斷了她的話頭。

「這位小姐，用不著你費心。我製造出來的東西，我自己會負責癱瘓掉。」

「……請問，你要怎麼癱瘓它們？」

「Lotus她們應該都知道，只要是我自己製造出來的強化外裝，我都可以用遙控的方式鎖住它們的保險，無論東西位在加速世界的哪裡，也不管是我從這裡製造的小眼球，還是小眼球自我繁殖再增加的小眼球都不例外。」

「…………！」

黑雪公主輕聲倒抽一口氣，才迅速搖搖頭說：

「我並不是忘了你的『遙控保險能力』……可是，ISS套件終端機上，根本沒貼上你那交叉雙槍的徽章啊。保險裝置的本體不就是徽章嗎？」

「我剛剛也說過，畢竟這些東西的造型不是我設計的。但只要是我製造的強化外裝，就一定會有保險。現在因為大眼球占了優勢，讓我沒辦法干涉那些小眼球，只要你們破壞大眼球，那一瞬間我就會鎖住所有小眼球的保險。因為即使變得像僵屍一樣，我還是有我的自尊。」

Rider語調平穩這麼宣告完，就再度轉身背向她們四人。他以裝模作樣的動作豎起大拇指，走向盤踞在樓層深處的巨大眼球。

黑雪公主朝著「槍匠」那絕不算高大的背影，拋出最後一句話：

「Rider！你……」

——你其實應該有話想說吧？我用卑鄙的偷襲手段，奪走你的超頻連線者生命，導致紅之團瓦解，你的虛擬角色身上難道都沒有累積半點對我的憤怒或怨恨嗎？

但她這句話哽在喉頭，說不出口。因為她知道問這個問題，只是想讓自己得到解脫。

於是黑雪公主只問了一個與這句話有點相似，主旨卻大不相同的問題。

「……你剛剛說過，說知道我三年前讓你損失所有點數的理由。你是怎麼知道的？」

「也不是說知道，只是從狀況推測，不過我想應該錯不了。」

Rider停下腳步，回答說：

「我剛剛也說過，我不記得是誰把我變成僵屍，說來是很遺憾。可是這個人應該就存在於進行七王會議的空間裡。然後有可能是湊巧Lotus讓我喪失所有點數，這人就正好讓我復活嗎？想也知道不可能，應該要想到這人是從一開始就做好了這樣的盤算。這人從一開始就想好了劇本，先慫恿黑之王拿下紅之王的首級，然後暗中將紅之王變成自己的傀儡……辦得到這種事情的…………」

Red　Rider說到這裡，微微轉過身來。他那形狀尖銳的護目鏡，被砍開大廣間的裂痕中射進的夕陽照得閃閃發光。

「……接下來你得自己找到答案。再見了，Lotus。還有『四大元素』的三個人，順便幫我跟『矛盾存在』問好。還有……幫我和繼承日珥的第二代頭目說，謝了，以後就拜託你了。」

初代紅之王再度舉起右手，這次將食指與中指並攏輕輕一揮，就踏進了巨大眼球的瞳孔。半透明黏膜將虛擬角色一寸寸吞沒，等到那泛黑的眼瞼慢慢眨下，再也沒留下半點Rider存在過的痕跡。

黑雪公主不明白充滿自己內心的是什麼情緒。一股不是恐懼、憤怒或悲傷，但包含了這一切的高壓能量，幾乎隨時都要沖破虛擬角色的裝甲而溢出。

「……小幸。」

楓子似乎察覺到這劍拔弩張的氣息，輕輕碰了碰她的背。黑雪公主深深吸氣、吐氣，然後壓抑住自己，對三名同伴宣告：

「——我們該做的事還是一樣。我們要使出渾身解數，破壞ISS套件本體。」

「……是啊，我們來這裡，就是為了這個目的。」

「我們一定，要辦到。」

「我們要加油！」

楓子說完後，晶與謠也出聲應和。

ISS套件本體既不是公敵，也不是強化外裝，而是對戰虛擬角色。

Red　Rider的影子是這麼告訴她們的。如果真是如此，那麼塑造出這個巨大眼球的人，就和黑雪公主她們一樣是有血有肉的人……同時還是年紀相近的少年或少女。而這個人就利用收進自rC體內的紅之王所具備的特殊能力，大量製造出注入黑暗心念的強化外裝，散播到加速世界之中。

黑雪公主等人不知道這是否是他或她所願，也有可能只是受到加速研究社操縱。然而即便真是受到操縱，事情演變到這個地步，也只能用武力解決了。超頻連線者一旦在戰場上與敵人對峙，唯一要做的就是一心一意地「對戰」。有些事情就是要打過才會了解、才能傳達給對方知道，哪怕對手是個不會說話的巨大眼球也不例外——

也不知道是不是對黑雪公主等人的決心起了反應，ISS套件本體的瞳孔再度變色。

從收進體內的傳送門發出的藍光，轉變為靜脈血液的暗紅色。

一陣沉重的嗡嗡聲響起，酷似腦髓的球體表層灑出了大量的黏稠黑暗。這種怎麼想都是由惡意化為可見現象的鬥氣，一路淹沒到二十公尺外擺出備戰架式的四人身上，讓她們產生了一種像是裝甲表面被無數尖針攬刺的感覺。

「難道說……這些全都是心念的過剩光……？」

楓子驚呼之餘，舉起右手。手掌上發出散發水藍色光芒的漣漪，把黑暗鬥氣推了回去。雖然過剩光本身並沒有攻擊力，但正向鬥氣會讓碰觸到的人感受到溫暖與鼓舞，相對的黑暗鬥氣則能以近似寒氣的效果，讓虛擬角色身體僵硬。

黑雪公主也和晶與謠同時以自身顏色的過剩光籠罩住身體，驅開了冰冷的黑暗。雖然不知道套件本體聽不聽得懂人話，但為防萬一，她還是小聲指揮：

「這眼球這麼大，動作應該不靈活！我們一口氣接近，繞到後面攻擊！」

三人齊聲回答了解，放低姿勢等黑雪公主下令衝鋒。

「衝……」

就在她正要喊出衝鋒命令之際……

籠罩住整顆眼球的鬥氣匯集在瞳孔上，化為一道漆黑的光束發射出來。

這是ISS套件特有的遠程攻擊型心念「黑暗氣彈」，但規模比終端機感染者所用的版本大了數十倍。

這股虛無的洪流就好像是屬性與大天使梅丹佐超高熱雷射相反的版本，將碰觸到的一切都削減、吞食、消滅，一路涌向黑雪公主等人，眼看就要將她們吞沒。
